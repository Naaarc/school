﻿using SchoolApi.ViewModels.VModels.VMDepartment;
using SchoolApi.ViewModels.VModels.VMDepartment.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Service.Interfaces
{
    public interface IDepartmentService
    {
        List<VMDepartmentList> GetAll(VMGetDepartmentListRequest vmRequest);
        VMDepartmentDetails Get(VMGetRemoveDepartmentRequest vmRequest);
        VMDepartmentDetails Create(VMCreateDepartmentRequest vmRequest);
        bool Remove(VMGetRemoveDepartmentRequest vmRequest);
        VMDepartmentDetails Update(VMUpdateDepartmentRequest vmRequest);
    }
}
