﻿using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMCourse.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Service.Interfaces
{
    public interface ICourseService
    {
        List<VMCourseList> GetAll(VMGetCourseListRequest vmRequest);
        VMCourseDetails Get(VMGetRemoveCourseDetailsRequest vmRequest);
        VMCourseDetails Create(VMCreateCourseRequest vmRequest);
        bool Remove(VMGetRemoveCourseDetailsRequest vmRequest);
        VMCourseDetails Update(VMUpdateCourseRequest vmRequest);
    }
}
