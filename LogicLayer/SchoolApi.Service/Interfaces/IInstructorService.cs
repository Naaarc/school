﻿using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMInstructor;
using SchoolApi.ViewModels.VModels.VMInstructor.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Service.Interfaces
{
    public interface IInstructorService
    {
        List<VMInstructorList> GetAll(VMGetInstructorsListRequest vmRequest);
        VMInstructorDetails Get(VMRemoveGetInstructorRequest vmRequest);
        VMInstructorDetails Create(VMCreateInstructorRequest vmRequest);
        bool Remove(VMRemoveGetInstructorRequest vmRequest);
        VMInstructorDetails Update(VMUpdateInstructorRequest vmRequest);
        //bool AssignCoursesToInstructor(int instructorId, ICollection<VMCourseDetails> courses);

    }
}
