﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Service.Interfaces
{
    public interface ISeedService
    {
        bool Seed();
    }
}
