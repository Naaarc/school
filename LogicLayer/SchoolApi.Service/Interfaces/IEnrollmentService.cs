﻿using SchoolApi.ViewModels.VModels.VMEnrollment;
using SchoolApi.ViewModels.VModels.VMEnrollment.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Service.Interfaces
{
    public interface IEnrollmentService
    {
        VMEnrollmentDetails Create(List<VMCreateEnrollmentRequest> vmRequest);
        bool Remove(VMRemoveDetailsEnrollmentRequest vmRequest);
    }
}
