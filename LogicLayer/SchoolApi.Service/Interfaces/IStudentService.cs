﻿using SchoolApi.ViewModels.VModels;
using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMCourse.Request;
using SchoolApi.ViewModels.VModels.VMStudent;
using SchoolApi.ViewModels.VModels.VMStudent.Request;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Service.Interfaces
{
    public interface IStudentService
    {
        List<VMStudentList> GetAll(GetStudentsListRequest request);
        VMStudentDetails Create(VMCreateStudentRequest vmRequest);
        bool Remove(VMGetStudentDetailsRemoveUpdateRequest vmRequest);
        VMStudentDetails Get(VMGetStudentDetailsRemoveUpdateRequest vmRequest);
        VMStudentDetails Update(VMUpdateStudentRequest vmRequest);
       
    }
}
