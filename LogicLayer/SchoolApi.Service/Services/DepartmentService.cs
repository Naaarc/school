﻿using Microsoft.EntityFrameworkCore;
using SchoolApi.Database;
using SchoolApi.Database.Models;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels.VModels.VMDepartment;
using SchoolApi.ViewModels.VModels.VMDepartment.Request;
using SchoolApi.ViewModels.VModels.VMInstructor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Service.Services
{
    public class DepartmentService : IDepartmentService
    {
        private readonly SchoolApiContext _context;

        public DepartmentService(SchoolApiContext context)
        {
            _context = context;
        }

        public List<VMDepartmentList> GetAll(VMGetDepartmentListRequest vmRequest)
        {
            List<VMDepartmentList> departments = _context.Departments
                .AsNoTracking()
                .Select(department => new VMDepartmentList
                {
                    DepartmentId = department.DepartmentID,
                    Name = department.Name,
                    Budget = department.Budget

                }).ToList();

            return departments;
        }

        public VMDepartmentDetails Get(VMGetRemoveDepartmentRequest vmRequest)
        {
            VMDepartmentDetails VMdepartment = _context.Departments
                .Where(department => department.DepartmentID == vmRequest.DepartmentId)
                .AsNoTracking()
                .Select(department => new VMDepartmentDetails
                {
                    DepartmentId = department.DepartmentID,
                    Name = department.Name,
                    Budget = department.Budget,
                    StartDate = department.StartDate,
                    InstructorId = department.InstructorID,
                    AdministratorName = department.Administrator.FullName

                }).First();
            if (VMdepartment == null)
            {
                throw new Exception("Department could not be found");
            }
            else
            {
                return VMdepartment;
            }
        }
        public VMDepartmentDetails Create(VMCreateDepartmentRequest vmRequest)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Department newDepartment = new Department
                    {
                        Name = vmRequest.Name,
                        Budget = vmRequest.Budget,
                        InstructorID = vmRequest.InstructorId,
                        StartDate = vmRequest.StartDate.ToLocalTime()
                    };
                    _context.Departments.Add(newDepartment);
                    _context.SaveChanges();

                    VMDepartmentDetails department = new VMDepartmentDetails
                    {
                        DepartmentId = newDepartment.DepartmentID,
                        Name = newDepartment.Name,
                        Budget = newDepartment.Budget,
                        StartDate = newDepartment.StartDate,
                        InstructorId = newDepartment.InstructorID
                    };

                    transaction.Commit();
                    return department;
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }

        public bool Remove(VMGetRemoveDepartmentRequest vmRequest)
        {
            Department department = _context.Departments
                .Where(department => department.DepartmentID == vmRequest.DepartmentId)
                .FirstOrDefault();
            if (department != null)
            {
                _context.Departments.Remove(department);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public VMDepartmentDetails Update(VMUpdateDepartmentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                throw new Exception("Student could not be found");
            }

            Department departmentToUpdate = _context.Departments
                .Where(department => department.DepartmentID == vmRequest.DepartmentId)
                .FirstOrDefault();

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    departmentToUpdate.DepartmentID = vmRequest.DepartmentId;
                    departmentToUpdate.Name = vmRequest.Name;
                    departmentToUpdate.StartDate = vmRequest.StartDate.ToLocalTime();
                    departmentToUpdate.InstructorID = vmRequest.InstructorId;
                    departmentToUpdate.Budget = vmRequest.Budget;

                    _context.SaveChanges();

                    var vmDepartment = new VMDepartmentDetails
                    {
                        DepartmentId = departmentToUpdate.DepartmentID,
                        InstructorId = departmentToUpdate.InstructorID,
                        Name = departmentToUpdate.Name,
                        Budget = departmentToUpdate.Budget,
                        StartDate = departmentToUpdate.StartDate
                    };

                    transaction.Commit();
                    return vmDepartment;
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }
    }
}
