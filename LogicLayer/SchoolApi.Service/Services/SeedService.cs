﻿using SchoolApi.Database;
using SchoolApi.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Service.Services
{
    public class SeedService : ISeedService
    {
        private readonly SchoolApiContext _context;
        public SeedService(SchoolApiContext context)
        {
            _context = context;
        }

        public bool Seed()
        {
            try
            {
                DataSeed.DoSeed(_context);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
