﻿using Microsoft.EntityFrameworkCore;
using SchoolApi.Database;
using SchoolApi.Database.Models;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMInstructor;
using SchoolApi.ViewModels.VModels.VMInstructor.Request;
using SchoolApi.ViewModels.VModels.VMOfficeAssignment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SchoolApi.Service.Services
{
    public class InstructorService : IInstructorService
    {
        private readonly SchoolApiContext _context;
        public InstructorService(SchoolApiContext context)
        {
            _context = context;
        }

        public List<VMInstructorList> GetAll(VMGetInstructorsListRequest vmRequest)
        {
            List<VMInstructorList> instructors = _context.Instructors
                .AsNoTracking()
                .Select(instructor => new VMInstructorList
                {
                    ID = instructor.ID,
                    FirstMidName = instructor.FirstMidName,
                    LastName = instructor.LastName,
                }).ToList();

            return instructors;
        }

        public VMInstructorDetails Get(VMRemoveGetInstructorRequest vmRequest)
        {
            VMInstructorDetails instructor = _context.Instructors
                .Where(instructor => instructor.ID == vmRequest.InstructorId)
                .AsNoTracking()
                .Select(instructor => new VMInstructorDetails
                {
                    ID = instructor.ID,
                    FirstMidName = instructor.FirstMidName,
                    LastName = instructor.LastName,
                    HireDate = instructor.HireDate,
                    CourseAssignments = instructor.CourseAssignments.Select(x => new VMCourseList
                    {
                        CourseId = x.CourseID,
                        Title = x.Course.Title,
                        Flag = false
                    }).ToList(),
                    OfficeAssignment = instructor.OfficeAssignment.Location

                }).First();

            if (instructor == null)
            {
                throw new Exception("Instructor could not be found");
            }
            else
            {
                return instructor;
            }
        }

        public VMInstructorDetails Create(VMCreateInstructorRequest vmRequest)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Instructor newInstructor = new Instructor
                    {
                        FirstMidName = vmRequest.FirstMidName,
                        LastName = vmRequest.LastName,
                        HireDate = vmRequest.HireDate.ToLocalTime(),

                    };
                    _context.Instructors.Add(newInstructor);
                    _context.SaveChanges();
                    this.AssignCoursesToInstructor(newInstructor.ID, vmRequest.CourseAssignments);
                    this.AssignOfficeToInstructor(newInstructor.ID, vmRequest.OfficeAssignment);

                    VMInstructorDetails instructor = new VMInstructorDetails
                    {
                        ID = newInstructor.ID,
                        FirstMidName = newInstructor.FirstMidName,
                        LastName = newInstructor.LastName,
                        HireDate = newInstructor.HireDate
                    };

                    transaction.Commit();
                    return instructor;
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }

        public VMInstructorDetails Update(VMUpdateInstructorRequest vmRequest)
        {
            if (vmRequest == null)
            {
                throw new Exception("Instructor could not be found");
            }

            Instructor instructorToUpdate = _context.Instructors
                .Where(instructor => instructor.ID == vmRequest.Id)
                .First();

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    this.AssignCoursesToInstructor(instructorToUpdate.ID, vmRequest.CourseAssignments);
                    this.AssignOfficeToInstructor(instructorToUpdate.ID, vmRequest.OfficeAssignment);

                    instructorToUpdate.ID = vmRequest.Id;
                    instructorToUpdate.FirstMidName = vmRequest.FirstMidName;
                    instructorToUpdate.LastName = vmRequest.LastName;
                    instructorToUpdate.HireDate = vmRequest.HireDate.ToLocalTime();

                    _context.SaveChanges();

                    var instructor = new VMInstructorDetails
                    {
                        ID = instructorToUpdate.ID,
                        FirstMidName = instructorToUpdate.FirstMidName,
                        LastName = instructorToUpdate.LastName,
                        HireDate = instructorToUpdate.HireDate
                    };
                    transaction.Commit();
                    return instructor;
                }
                catch(Exception exc) 
                {
                    throw exc;
                }
            }
        }
        public bool Remove(VMRemoveGetInstructorRequest vmRequest)
        {
            Instructor instructor = _context.Instructors
                .Where(instructor => instructor.ID == vmRequest.InstructorId)
                .First();

            if (instructor != null)
            {
                _context.Instructors.Remove(instructor);
                _context.SaveChanges();
                return true;
            }
            return false;
        }

        public bool AssignCoursesToInstructor(int instructorId, ICollection<VMCourseDetails> courses)
        {
            var toClear = _context.CourseAssignments.Where(x => x.InstructorID == instructorId).ToList();
            if (toClear != null)
            {
                foreach (var item in toClear)
                {
                    _context.CourseAssignments.Remove(item);
                }
            }
            if (courses != null)
            {
                foreach (VMCourseList course in courses)
                {
                    if (course.Flag == true)
                    {
                        var courseAssignment = new CourseAssignment
                        {
                            InstructorID = instructorId,
                            CourseID = course.CourseId
                        };
                        _context.CourseAssignments.Add(courseAssignment);
                    }
                    
                }
            }
            _context.SaveChanges();
            return true;
        }

        public bool AssignOfficeToInstructor(int instructorId, string location)
        {
            var toClear = _context.OfficeAssignments.Where(x => x.InstructorID == instructorId).FirstOrDefault();
            if (toClear != null)
            {
                _context.OfficeAssignments.Remove(toClear);
            }
            var officeAssignment = new OfficeAssignment
            {
                InstructorID = instructorId,
                Location = location
            };
            _context.OfficeAssignments.Add(officeAssignment);
            _context.SaveChanges();
            return true;
        }
    }
}
