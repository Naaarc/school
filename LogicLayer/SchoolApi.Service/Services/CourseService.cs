﻿using SchoolApi.Database;
using SchoolApi.Database.Models;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMCourse.Request;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;

namespace SchoolApi.Service.Services
{
    public class CourseService : ICourseService
    {
        private readonly SchoolApiContext _context;
        public CourseService(SchoolApiContext context)
        {
            _context = context;
        }
        public List<VMCourseList> GetAll(VMGetCourseListRequest vmRequest)
        {
            List<VMCourseList> courses = _context.Courses
                .Select(x => new VMCourseList
                {
                    CourseId = x.CourseID,
                    Title = x.Title,
                    Credits = x.Credits,
                    DepartmentId = x.DepartmentID,
                    DepartmentName = x.Department.Name


                }).ToList();

            return courses;
        }

        public VMCourseDetails Get(VMGetRemoveCourseDetailsRequest vmRequest)
        {
            VMCourseDetails course = _context.Courses
                .Where(course => course.CourseID == vmRequest.CourseId)
                .AsNoTracking()
                .Select(course => new VMCourseDetails
                {
                    CourseId = course.CourseID,
                    Title = course.Title,
                    Credits = course.Credits,
                    DepartmentId = course.DepartmentID,
                    DepartmentName = course.Department.Name

                }).FirstOrDefault();

            if (course == null)
            {
                throw new Exception("Course could not be found");
            }
            else
                return course;
        }

        public VMCourseDetails Create(VMCreateCourseRequest vmRequest)
        {
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Course newCourse = new Course
                    {

                        Title = vmRequest.Title,
                        Credits = vmRequest.Credits,
                        DepartmentID = vmRequest.DepartmentId

                    };
                    _context.Courses.Add(newCourse);
                    _context.SaveChanges();


                    VMCourseDetails course = new VMCourseDetails
                    {
                        CourseId = newCourse.CourseID,
                        Title = newCourse.Title,
                        Credits = newCourse.Credits,
                        DepartmentId = newCourse.DepartmentID

                    };

                    transaction.Commit();
                    return course;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        public bool Remove(VMGetRemoveCourseDetailsRequest vmRequest)
        {
            Course course = _context.Courses
                .Where(course => course.CourseID == vmRequest.CourseId)
                .FirstOrDefault();

            if (course != null)
            {
                _context.Courses.Remove(course);
                _context.SaveChanges();
                return true;
            }

            return false;
        }

        public VMCourseDetails Update(VMUpdateCourseRequest vmRequest)
        {
            if (vmRequest == null)
            {
                throw new Exception("Course could not be found");
            }

            Course courseToUpdate = _context.Courses
                .Where(course => course.CourseID == vmRequest.CourseId)
                .FirstOrDefault();

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    courseToUpdate.CourseID = vmRequest.CourseId;
                    courseToUpdate.DepartmentID = vmRequest.DepartmentId;
                    courseToUpdate.Credits = vmRequest.Credits;
                    courseToUpdate.Title = vmRequest.Title;
                    _context.SaveChanges();

                    var vmCourse = new VMCourseDetails
                    {
                        CourseId = courseToUpdate.CourseID,
                        DepartmentId = courseToUpdate.DepartmentID,
                        Title = courseToUpdate.Title,
                        Credits = courseToUpdate.Credits,
                    };

                    transaction.Commit();
                    return vmCourse;
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }

    }
}
