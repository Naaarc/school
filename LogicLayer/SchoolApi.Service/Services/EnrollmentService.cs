﻿using SchoolApi.Database;
using SchoolApi.Database.Models;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels.VModels.VMEnrollment;
using SchoolApi.ViewModels.VModels.VMEnrollment.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Transactions;

namespace SchoolApi.Service.Services
{
    public class EnrollmentService : IEnrollmentService
    {
        private readonly SchoolApiContext _context;
        public EnrollmentService(SchoolApiContext context)
        {
            _context = context;
        }

        public VMEnrollmentDetails Create(List<VMCreateEnrollmentRequest> vmRequest)
        {


            List<VMEnrollmentDetails> existingEnrollments = _context.Enrollments
                .Select(x => new VMEnrollmentDetails
                {
                    StudentId = x.StudentID,
                    CourseId = x.CourseID

                }).ToList();

            //foreach (var i in existingEnrollments)
            //{
            //    foreach (var y in vmRequest)
            //    {
            //        if (i.StudentId == y.StudentId && i.CourseId == y.CourseId)
            //        {
            //            return null;
            //        }
            //    }
            //}

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    foreach (var i in vmRequest)
                    {
                        if (i.Flag == true)
                        {
                            Enrollment newEnrollment = new Enrollment
                            {
                                StudentID = i.StudentId,
                                CourseID = i.CourseId

                            };
                            _context.Enrollments.Add(newEnrollment);
                        }
                    }
                    _context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
            return null;
        }

        public bool Remove(VMRemoveDetailsEnrollmentRequest vmRequest)
        {

            Enrollment enrollment = _context.Enrollments
                .Where(x => x.StudentID == vmRequest.StudentId && x.CourseID == vmRequest.CourseId && vmRequest.Flag == false)
                .FirstOrDefault();

            if (enrollment != null)
            {
                _context.Enrollments.Remove(enrollment);
                _context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
