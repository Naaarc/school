﻿using Microsoft.EntityFrameworkCore;
using SchoolApi.Database;
using SchoolApi.Database.Models;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels.VModels;
using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMCourse.Request;
using SchoolApi.ViewModels.VModels.VMStudent;
using SchoolApi.ViewModels.VModels.VMStudent.Request;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;

namespace SchoolApi.Service.Services
{
    public class StudentService :IStudentService
    {
        private readonly SchoolApiContext _context;

        public StudentService(SchoolApiContext context)
        {
            _context = context;
        }

        public List<VMStudentList> GetAll(GetStudentsListRequest vmRequest)
        {
            List<VMStudentList> students = _context.Students
                .AsNoTracking()
                .Select(x => new VMStudentList
                {
                    ID = x.ID,
                    FirstMidName = x.FirstMidName,
                    LastName = x.LastName

                }).ToList();
            return students;
        }
        public VMStudentDetails Get(VMGetStudentDetailsRemoveUpdateRequest vmRequest)
        {
            VMStudentDetails student = _context.Students
                .Where(x => x.ID == vmRequest.ID)
                .AsNoTracking()
                .Select(x => new VMStudentDetails
                {
                    ID = x.ID,
                    FirstMidName = x.FirstMidName,
                    LastName = x.LastName,
                    EnrollmentDate = x.EnrollmentDate,
                    Courses = x.Enrollments.Select(x => new VMCourseList
                    {
                        CourseId = x.CourseID,
                        Title = x.Course.Title,
                        Flag = false

                    }).ToList()
                }).FirstOrDefault();

            if (student == null)
            {
                throw new Exception("Student could not be found");
            }
            else 
                return student;
        }

        public VMStudentDetails Create(VMCreateStudentRequest vmRequest)
        {
            //new TransactionExecutor()
            //    .execute(() =>
            //    {
            //        Student newStudent = new Student
            //        {
            //            LastName = vmRequest.LastName,
            //            FirstMidName = vmRequest.FirstMidName,
            //            EnrollmentDate = vmRequest.EnrollmentDate.ToLocalTime()
            //        };
            //        _context.Students.Add(newStudent);
            //        _context.SaveChanges();


            //        VMStudentDetails student = new VMStudentDetails
            //        {
            //            ID = newStudent.ID,
            //            FirstMidName = newStudent.FirstMidName,
            //            LastName = newStudent.LastName,
            //            EnrollmentDate = newStudent.EnrollmentDate
            //        };
            //    });
            //class TransactionExecutor { 
            //public void execute(doStuff)
            //{
            //    using (var transaction = _context.Database.BeginTransaction())
            //    {
            //        try
            //        {
            //            doStuff();

            //            transaction.Commit();
            //            return student;
            //        }
            //        catch (Exception ex)
            //        {
            //            transaction.Rollback();
            //            throw ex;
            //        }
            //    }
            //}
            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Student newStudent = new Student
                    {
                        LastName = vmRequest.LastName,
                        FirstMidName = vmRequest.FirstMidName,
                        EnrollmentDate = vmRequest.EnrollmentDate.ToLocalTime()
                    };
                    _context.Students.Add(newStudent);
                    _context.SaveChanges();


                    VMStudentDetails student = new VMStudentDetails
                    {
                        ID = newStudent.ID,
                        FirstMidName = newStudent.FirstMidName,
                        LastName = newStudent.LastName,
                        EnrollmentDate = newStudent.EnrollmentDate
                    };

                    transaction.Commit();
                    return student;
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    throw ex;
                }
            }
        }

        public bool Remove(VMGetStudentDetailsRemoveUpdateRequest vmRequest)
        {
            Student student = _context.Students
                .Where(x => x.ID == vmRequest.ID)
                .FirstOrDefault();

            if (student != null)
            {
                _context.Students.Remove(student);
                _context.SaveChanges();
                return true;
            }
            return false;

        }
        public VMStudentDetails Update(VMUpdateStudentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                throw new Exception("Student could not be found");
            }

            Student studentToUpdate =  _context.Students
                .Where(x => x.ID == vmRequest.ID)
                .FirstOrDefault();

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    studentToUpdate.ID = vmRequest.ID;
                    studentToUpdate.FirstMidName = vmRequest.FirstMidName;
                    studentToUpdate.LastName = vmRequest.LastName;
                    studentToUpdate.EnrollmentDate = vmRequest.EnrollmentDate;
                    _context.SaveChanges();

                    var vmStudent = new VMStudentDetails
                    {
                        ID = studentToUpdate.ID,
                        FirstMidName = studentToUpdate.FirstMidName,
                        LastName = studentToUpdate.LastName,
                        EnrollmentDate = studentToUpdate.EnrollmentDate,
                    };

                    transaction.Commit();
                    return vmStudent;
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }

        }
    }
}
