﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels
{
    [DataContract]
    public class BooleanResponse : BaseResponse
    {
        [DataMember]
        public bool Value { get; private set; }

        public BooleanResponse()
        {
            Value = false;
        }

        public BooleanResponse(bool value)
        {
            Value = value;
        }
    }
}
