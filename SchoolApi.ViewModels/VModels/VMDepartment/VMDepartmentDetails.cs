﻿using SchoolApi.ViewModels.VModels.VMInstructor;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMDepartment
{
    [DataContract]
    public class VMDepartmentDetails : VMDepartmentList
    {
        [DataMember]
        public DateTime StartDate { get; set; }

        [DataMember]
        public int? InstructorId { get; set; }

        [DataMember]
        public string AdministratorName { get; set; }

    }
}
