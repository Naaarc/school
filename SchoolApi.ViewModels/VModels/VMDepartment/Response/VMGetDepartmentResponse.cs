﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMDepartment.Response
{
    [DataContract]
    public class VMGetDepartmentResponse : BaseResponse
    {
        [DataMember]
        public VMDepartmentDetails Department { get; set; }
    }
}
