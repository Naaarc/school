﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMDepartment.Response
{
    [DataContract]
    public class VMGetDepartmentListResponse
    {
        [DataMember]
        public ICollection<VMDepartmentList> Departments { get; set; }
    }
}
