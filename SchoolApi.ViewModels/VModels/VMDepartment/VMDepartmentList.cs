﻿using SchoolApi.ViewModels.VModels.VMDepartment.Response;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMDepartment
{
    [DataContract]
    public class VMDepartmentList
    {
        [DataMember]
        public int DepartmentId { get; set; }

        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal Budget { get; set; }

        public static VMGetDepartmentListResponse ToResponse(List<VMDepartmentList> vmbsic)
        {
            var vmResponse = new VMGetDepartmentListResponse
            {
                Departments = vmbsic
            };
            return vmResponse;
        }

        public static VMGetDepartmentResponse ToResponse(VMDepartmentDetails vmbsic)
        {
            var vmResponse = new VMGetDepartmentResponse
            {
                Department = vmbsic
            };
            return vmResponse;
        }
    }
}
