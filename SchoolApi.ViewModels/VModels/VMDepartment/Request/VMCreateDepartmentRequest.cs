﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMDepartment.Request
{
    [DataContract]
    public class VMCreateDepartmentRequest
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public decimal Budget { get; set; }

        [DataMember]
        public DateTime StartDate { get; set; }
        
        [DataMember]
        public int InstructorId { get; set; }
    }
}
