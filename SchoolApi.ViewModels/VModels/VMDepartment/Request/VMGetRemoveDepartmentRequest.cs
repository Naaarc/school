﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMDepartment.Request
{
    [DataContract]
    public class VMGetRemoveDepartmentRequest
    {
        [DataMember]
        public int DepartmentId { get; set; }
    }
}
