﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMStudent.Response
{
    [DataContract]
    public class VMStudentResponse : BaseResponse
    {
        [DataMember]
        public VMStudentDetails Student { get; set; }

    }
}
