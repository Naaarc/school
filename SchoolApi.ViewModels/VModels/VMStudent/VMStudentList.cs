﻿using SchoolApi.ViewModels.VModels.VMPerson;
using SchoolApi.ViewModels.VModels.VMStudent;
using SchoolApi.ViewModels.VModels.VMStudent.Response;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMStudent
{
    [DataContract]
    public class VMStudentList : VMPersonList
    {
        public static VMGetStudentsListResponse ToResponse(List<VMStudentList> vmbsic)
        {
            var vmResponse = new VMGetStudentsListResponse
            {
                Students = vmbsic
            };
            return vmResponse;
        }

        public static VMStudentResponse ToResponse(VMStudentDetails vmbsic)
        {
            var vmResponse = new VMStudentResponse
            {
                Student = vmbsic
            };
            return vmResponse;
        }

    }
}
