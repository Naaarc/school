﻿using SchoolApi.ViewModels.VModels.VMCourse;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMStudent
{
    [DataContract]
    public class VMStudentDetails : VMStudentList
    {
        [DataMember]
        public DateTime? EnrollmentDate { get; set; }

        [DataMember]
        public ICollection<VMCourseList> Courses { get; set; }

    }
}
