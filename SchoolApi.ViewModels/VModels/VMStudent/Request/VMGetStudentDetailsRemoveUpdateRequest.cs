﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMStudent.Request
{
    [DataContract]
    public class VMGetStudentDetailsRemoveUpdateRequest
    {
        [DataMember]
        public int ID { get; set; }
    }
}
