﻿using SchoolApi.Database.Models;
using SchoolApi.ViewModels.VModels.VMEnrollment;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMStudent.Request
{
    [DataContract]
    public class VMCreateStudentRequest
    {
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FirstMidName { get; set; }
        [DataMember]
        public DateTime EnrollmentDate { get; set; }

    }
}
