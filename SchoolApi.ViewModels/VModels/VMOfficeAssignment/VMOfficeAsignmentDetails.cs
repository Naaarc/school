﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMOfficeAssignment
{
    [DataContract]
    public class VMOfficeAsignmentDetails
    {
        [DataMember]
        public int InstructorId { get; set; }

        [DataMember]
        public string Location { get; set; }
    }
}
