﻿
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMEnrollment.Response
{
    [DataContract]
    public class VMEnrollmentResponse : BaseResponse
    {
        public VMEnrollmentDetails Enrollment { get; set; }
    }
}
