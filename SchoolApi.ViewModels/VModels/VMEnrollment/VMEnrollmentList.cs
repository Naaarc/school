﻿using SchoolApi.ViewModels.VModels.VMEnrollment.Response;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMEnrollment
{
    [DataContract]
    public class VMEnrollmentList
    {
        [DataMember]
        public int EnrollmentId { get; set; }

        public static VMEnrollmentResponse ToResponse(VMEnrollmentDetails vmbsic)
        {
            var vmResponse = new VMEnrollmentResponse
            {
                Enrollment = vmbsic
            };
            return vmResponse;
        }

    }
}
