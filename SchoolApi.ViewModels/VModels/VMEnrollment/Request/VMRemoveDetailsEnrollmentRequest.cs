﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMEnrollment.Request
{
    [DataContract]
    public class VMRemoveDetailsEnrollmentRequest
    {

        [DataMember]
        public int StudentId { get; set; }
        [DataMember]
        public int CourseId { get; set; }
        [DataMember]
        public Boolean Flag { get; set; }
    }
}
