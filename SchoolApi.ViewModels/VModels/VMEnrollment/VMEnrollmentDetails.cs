﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMEnrollment
{
    [DataContract]
    public class VMEnrollmentDetails : VMEnrollmentList
    {

        [DataMember]
        public int CourseId { get; set; }
        [DataMember]
        public int StudentId { get; set; }
        [DataMember]
        public Boolean Flag { get; set; }
    }
}
