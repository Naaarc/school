﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMCourseAssignment
{
    [DataContract]
    public class VMCourseAssignments
    {
        [DataMember]
        public int CourseId { get; set; }

        [DataMember]
        public int InstructorId { get; set; }
    }
}
