﻿using SchoolApi.ViewModels.VModels.VMCourse.Response;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMCourse
{
    [DataContract]
    public class VMCourseList
    {
        [DataMember]
        public int CourseId { get; set; }

        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int Credits { get; set; }

        [DataMember]
        public Boolean Flag { get; set; }

        [DataMember]
        public int DepartmentId { get; set; }

        [DataMember]
        public string DepartmentName { get; set; }

        public static VMGetCourseListResponse ToResponse(List<VMCourseList> vmbsic)
        {
            var vmResponse = new VMGetCourseListResponse
            {
                Courses = vmbsic
            };
            return vmResponse;
        }

        public static VMGetCourseResponse ToResponse(VMCourseDetails vmbsic)
        {
            var vmResponse = new VMGetCourseResponse
            {
                Course = vmbsic
            };
            return vmResponse;
        }
    }
}
