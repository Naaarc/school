﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMCourse.Response
{
    [DataContract]
    public class VMGetCourseResponse : BaseResponse
    {
        [DataMember]
        public VMCourseDetails Course { get; set; }
    }
}
