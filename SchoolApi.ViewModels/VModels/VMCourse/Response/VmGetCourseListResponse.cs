﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMCourse.Response
{
    public class VMGetCourseListResponse : BaseResponse
    {
        public ICollection<VMCourseList> Courses { get; set; }
    }
}
