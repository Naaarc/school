﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMCourse.Request
{
    
    [DataContract]
    public class VMUpdateCourseRequest : VMCreateCourseRequest
    {
        [DataMember]
        public int CourseId { get; set; }
    }
}
