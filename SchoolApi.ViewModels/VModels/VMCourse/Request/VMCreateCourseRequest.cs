﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMCourse.Request
{
    [DataContract]
    public class VMCreateCourseRequest
    {
        [DataMember]
        public string Title { get; set; }

        [DataMember]
        public int Credits { get; set; }

        [DataMember]
        public int DepartmentId { get; set; }
    }
}
