﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMPerson
{
    [DataContract]
    public class VMPersonList : VMPersonDetails
    {
        [DataMember]
        public int ID { get; set; }

    }
}
