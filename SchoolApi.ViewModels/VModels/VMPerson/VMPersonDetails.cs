﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMPerson
{
    [DataContract]
    public class VMPersonDetails
    {
        [DataMember]
        public string LastName { get; set; }
        [DataMember]
        public string FirstMidName { get; set; }

        [DataMember]
        public string FullName
        {
            get
            {
                return LastName + ", " + FirstMidName;
            }
        }
    }
}
