﻿using SchoolApi.Database.Models;
using SchoolApi.ViewModels.VModels.VMInstructor.Response;
using SchoolApi.ViewModels.VModels.VMPerson;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMInstructor
{
    [DataContract]
    public class VMInstructorList : VMPersonList
    {
        //[DataMember]
        //public DateTime HireDate { get; set; }

        public static VMGetInstructorsListResponse ToResponse(List<VMInstructorList> vmRequest)
        {
            var vmResponse = new VMGetInstructorsListResponse
            {
                Instructors = vmRequest
            };

            return vmResponse;
        }

        public static VMGetInstructorResponse ToResponse(VMInstructorDetails vmRequest)
        {
            var vmResponse = new VMGetInstructorResponse

            {
                Instructor = vmRequest
            };

            return vmResponse;
        }
    }
}
