﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMInstructor.Response
{
    [DataContract]
    public class VMGetInstructorsListResponse : BaseResponse
    {
        [DataMember]
        public ICollection<VMInstructorList> Instructors { get; set; }
    }
}
