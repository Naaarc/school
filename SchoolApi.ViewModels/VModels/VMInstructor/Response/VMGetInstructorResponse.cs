﻿using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMInstructor.Response
{
    [DataContract]
    public class VMGetInstructorResponse
    {
        public VMInstructorDetails Instructor { get; set; }
    }
}
