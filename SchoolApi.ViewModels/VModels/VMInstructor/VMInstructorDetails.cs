﻿using SchoolApi.Database.Models;
using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMCourseAssignment;
using SchoolApi.ViewModels.VModels.VMOfficeAssignment;
using SchoolApi.ViewModels.VModels.VMPerson;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMInstructor
{
    [DataContract]
    public class VMInstructorDetails : VMInstructorList
    {
        [DataMember]
        public DateTime HireDate { get; set; }

        [DataMember]
        public ICollection<VMCourseList> CourseAssignments { get; set; }

        [DataMember]
        public string OfficeAssignment { get; set; }
    }
}
