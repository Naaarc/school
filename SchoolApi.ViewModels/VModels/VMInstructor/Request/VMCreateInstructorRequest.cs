﻿using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMOfficeAssignment;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMInstructor.Request
{
    [DataContract]
    public class VMCreateInstructorRequest
    {
        [DataMember]
        public string FirstMidName { get; set; }

        [DataMember]
        public string LastName { get; set; }

        [DataMember]
        public DateTime HireDate { get; set; }

        [DataMember]
        public string OfficeAssignment { get; set; }

        [DataMember]
        public ICollection<VMCourseDetails> CourseAssignments { get; set; }
    }
}
