﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace SchoolApi.ViewModels.VModels.VMInstructor.Request
{
    [DataContract]
    public class VMUpdateInstructorRequest : VMCreateInstructorRequest
    {
        [DataMember]
        public int Id { get; set; }
    }
}
