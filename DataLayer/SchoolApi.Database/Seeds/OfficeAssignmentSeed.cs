﻿using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Database.Seeds
{
    public static class OfficeAssignmentSeed
    {
        public static void DoSeed(SchoolApiContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var instructors = new List<Instructor>();
                    foreach (var i in context.Instructors)
                    {
                        instructors.Add(i);
                    }
                    var officeAssignments = new OfficeAssignment[]
                    {
                        new OfficeAssignment {
                            InstructorID = instructors.Single( i => i.LastName == "Fakhouri").ID,
                            Location = "Smith 17" },
                        new OfficeAssignment {
                            InstructorID = instructors.Single( i => i.LastName == "Harui").ID,
                            Location = "Gowan 27" },
                        new OfficeAssignment {
                            InstructorID = instructors.Single( i => i.LastName == "Kapoor").ID,
                            Location = "Thompson 304" },
                    };

                    foreach (OfficeAssignment o in officeAssignments)
                    {
                        context.OfficeAssignments.Add(o);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }
    }
}
