﻿using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Database.Seeds
{
    public static class EnrollmentSeed
    {
        public static void DoSeed(SchoolApiContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var students = new List<Student>();
                    var courses = new List<Course>();

                    foreach (var i in context.Students)
                    {
                        students.Add(i);
                    }

                    foreach (var c in context.Courses)
                    {
                        courses.Add(c);
                    }

                    var enrollments = new Enrollment[]
                    {
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Alexander").ID,
                            CourseID = courses.Single(c => c.Title == "Chemistry" ).CourseID,
                            Grade = Grade.A
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Alexander").ID,
                            CourseID = courses.Single(c => c.Title == "Microeconomics" ).CourseID,
                            Grade = Grade.C
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Alexander").ID,
                            CourseID = courses.Single(c => c.Title == "Macroeconomics" ).CourseID,
                            Grade = Grade.B
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Alonso").ID,
                            CourseID = courses.Single(c => c.Title == "Calculus" ).CourseID,
                            Grade = Grade.B
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Alonso").ID,
                            CourseID = courses.Single(c => c.Title == "Trigonometry" ).CourseID,
                            Grade = Grade.B
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Alonso").ID,
                            CourseID = courses.Single(c => c.Title == "Composition" ).CourseID,
                            Grade = Grade.B
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Anand").ID,
                            CourseID = courses.Single(c => c.Title == "Chemistry" ).CourseID
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Anand").ID,
                            CourseID = courses.Single(c => c.Title == "Microeconomics").CourseID,
                            Grade = Grade.B
                        },
                         new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Barzdukas").ID,
                            CourseID = courses.Single(c => c.Title == "Chemistry").CourseID,
                            Grade = Grade.B
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Li").ID,
                            CourseID = courses.Single(c => c.Title == "Composition").CourseID,
                            Grade = Grade.B
                        },
                        new Enrollment {
                            StudentID = students.Single(s => s.LastName == "Justice").ID,
                            CourseID = courses.Single(c => c.Title == "Literature").CourseID,
                            Grade = Grade.B
                        }
                    };

                    foreach (Enrollment e in enrollments)
                    {
                        var enrollmentInDataBase = context.Enrollments
                            .Where(s => s.Student.ID == e.StudentID && s.Course.CourseID == e.CourseID)
                            .SingleOrDefault();

                        if (enrollmentInDataBase == null)
                        {
                            context.Enrollments.Add(e);
                        }
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }
    }
}
