﻿using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Database.Seeds
{
    public static class StudentSeed
    {
        public static void DoSeed(SchoolApiContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                Seed(context);
                transaction.Commit();
            }
        }

        private static void Seed(SchoolApiContext context)
        {
            try
            {
                if (context.Students.Any())
                {
                    return;
                }
                var students = new Student[]
                {
                        new Student { FirstMidName = "Carson",   LastName = "Alexander",
                            EnrollmentDate = DateTime.Parse("2010-09-01") },
                        new Student { FirstMidName = "Meredith", LastName = "Alonso",
                            EnrollmentDate = DateTime.Parse("2012-09-01") },
                        new Student { FirstMidName = "Arturo",   LastName = "Anand",
                            EnrollmentDate = DateTime.Parse("2013-09-01") },
                        new Student { FirstMidName = "Gytis",    LastName = "Barzdukas",
                            EnrollmentDate = DateTime.Parse("2012-09-01") },
                        new Student { FirstMidName = "Yan",      LastName = "Li",
                            EnrollmentDate = DateTime.Parse("2012-09-01") },
                        new Student { FirstMidName = "Peggy",    LastName = "Justice",
                            EnrollmentDate = DateTime.Parse("2011-09-01") },
                        new Student { FirstMidName = "Laura",    LastName = "Norman",
                            EnrollmentDate = DateTime.Parse("2013-09-01") },
                        new Student { FirstMidName = "Nino",     LastName = "Olivetto",
                            EnrollmentDate = DateTime.Parse("2005-09-01") }
                };


                foreach (Student s in students)
                {
                    context.Students.Add(s);
                    Console.WriteLine(s);
                }

                context.SaveChanges();
            }
            catch (Exception exc)
            {
                throw exc;
            }
        }
    }
}
