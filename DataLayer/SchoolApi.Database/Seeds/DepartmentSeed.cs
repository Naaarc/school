﻿using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Database.Seeds
{
    public static class DepartmentSeed
    {
        public static void DoSeed(SchoolApiContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var instructors = new List<Instructor>();
                    foreach (var i in context.Instructors)
                    {
                        instructors.Add(i);
                    }

                    var departments = new Department[]
                    {
                        new Department { Name = "English",     Budget = 350000,
                            StartDate = DateTime.Parse("2007-09-01"),
                            InstructorID  = instructors.Single( i => i.LastName == "Abercrombie").ID},
                        new Department { Name = "Mathematics", Budget = 100000,
                            StartDate = DateTime.Parse("2007-09-01"),
                            InstructorID  = instructors.Single( i => i.LastName == "Fakhouri").ID },
                        new Department { Name = "Engineering", Budget = 350000,
                            StartDate = DateTime.Parse("2007-09-01"),
                            InstructorID  = instructors.Single( i => i.LastName == "Harui").ID },
                        new Department { Name = "Economics",   Budget = 100000,
                            StartDate = DateTime.Parse("2007-09-01"),
                            InstructorID  = instructors.Single( i => i.LastName == "Kapoor").ID }
                    };

                    foreach (Department d in departments)
                    {
                        context.Departments.Add(d);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }
    }
}

