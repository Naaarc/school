﻿using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Database.Seeds
{
    public static class CourseSeed
    {
        public static void DoSeed(SchoolApiContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {
                    var departments = new List<Department>();
                    foreach (var d in context.Departments)
                    {
                        departments.Add(d);
                    }

                    var courses = new Course[]
                    {
                        new Course {CourseID = 1050, Title = "Chemistry",      Credits = 3,
                            DepartmentID = departments.Single( s => s.Name == "Engineering").DepartmentID},
                        new Course {CourseID = 4022, Title = "Microeconomics", Credits = 3,
                            DepartmentID = departments.Single( s => s.Name == "Economics").DepartmentID},
                        new Course {CourseID = 4041, Title = "Macroeconomics", Credits = 3,
                            DepartmentID = departments.Single( s => s.Name == "Economics").DepartmentID},
                        new Course {CourseID = 1045, Title = "Calculus",       Credits = 4,
                            DepartmentID = departments.Single( s => s.Name == "Mathematics").DepartmentID},
                        new Course {CourseID = 3141, Title = "Trigonometry",   Credits = 4,
                            DepartmentID = departments.Single( s => s.Name == "Mathematics").DepartmentID},
                        new Course {CourseID = 2021, Title = "Composition",    Credits = 3,
                            DepartmentID = departments.Single( s => s.Name == "English").DepartmentID},
                        new Course {CourseID = 2042, Title = "Literature",     Credits = 4,
                            DepartmentID = departments.Single( s => s.Name == "English").DepartmentID},
                    };

                    foreach (Course c in courses)
                    {
                        context.Courses.Add(c);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }
    }
}
