﻿using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Database.Seeds
{
    public static class CourseAssignmentSeed
    {
        public static void DoSeed(SchoolApiContext context)
        {
            using (var transaction = context.Database.BeginTransaction())
            {
                try
                {

                    var courses = new List<Course>();
                    var instructors = new List<Instructor>();
                    foreach (var c in context.Courses)
                    {
                        courses.Add(c);
                    }
                    foreach (var i in context.Instructors)
                    {
                        instructors.Add(i);
                    }

                    var courseInstructors = new CourseAssignment[]
                    {
                        new CourseAssignment {
                            CourseID = courses.Single(c => c.Title == "Chemistry" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Kapoor").ID},
                        new CourseAssignment {
                            CourseID = courses.Single(c => c.Title == "Chemistry" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Harui").ID},
                        new CourseAssignment {
                            CourseID = courses.Single(c => c.Title == "Microeconomics" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Zheng").ID},
                        new CourseAssignment{
                            CourseID = courses.Single(c => c.Title == "Macroeconomics" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Zheng").ID},
                        new CourseAssignment {
                            CourseID = courses.Single(c => c.Title == "Calculus" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Fakhouri").ID},
                        new CourseAssignment {
                            CourseID = courses.Single(c => c.Title == "Trigonometry" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Harui").ID},
                        new CourseAssignment {
                            CourseID = courses.Single(c => c.Title == "Composition" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Abercrombie").ID},
                        new CourseAssignment {
                            CourseID = courses.Single(c => c.Title == "Literature" ).CourseID,
                            InstructorID = instructors.Single(i => i.LastName == "Abercrombie").ID},
                    };

                    foreach (CourseAssignment ci in courseInstructors)
                    {
                        context.CourseAssignments.Add(ci);
                    }
                    context.SaveChanges();
                    transaction.Commit();
                }
                catch (Exception exc)
                {
                    transaction.Rollback();
                    throw exc;
                }
            }
        }
    }
}
