﻿using Microsoft.EntityFrameworkCore;
using SchoolApi.Database.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolApi.Database
{
    public class SchoolApiContext : DbContext
    {
        public SchoolApiContext(DbContextOptions<SchoolApiContext> options) : base(options) 
        { }

        public DbSet<Course> Courses { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Instructor> Instructors { get; set; }
        public DbSet<OfficeAssignment> OfficeAssignments { get; set; }
        public DbSet<CourseAssignment> CourseAssignments { get; set; }
        public DbSet<Person> People { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Course>().ToTable("Course");
            modelBuilder.Entity<Person>().ToTable("Person");
            modelBuilder.Entity<Enrollment>().ToTable("Enrollment");
            //.hasBaseType adding for Inheritance class
            modelBuilder.Entity<Student>()
                .HasBaseType<Person>();

            modelBuilder.Entity<Department>().ToTable("Department");
            modelBuilder.Entity<Instructor>()
                .HasBaseType<Person>();

            //var entityTypes = modelBuilder.Model.GetEntityTypes().ToList();

            // https://github.com/aspnet/EntityFrameworkCore/issues/18006
            //foreach (var entityType in entityTypes)
            //{
            //    if (entityType.BaseType != null)
            //    {
            //        modelBuilder.Ignore(entityType.ClrType);
            //    }
            //}
            modelBuilder.Entity<OfficeAssignment>().ToTable("OfficeAssignment");
            modelBuilder.Entity<OfficeAssignment>().HasKey(c => new { c.InstructorID });

            modelBuilder.Entity<CourseAssignment>().ToTable("CourseAssignment");
            modelBuilder.Entity<CourseAssignment>().HasKey(c => new { c.CourseID, c.InstructorID });
        }
    }
}