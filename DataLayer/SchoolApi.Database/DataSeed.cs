﻿using Microsoft.EntityFrameworkCore;
using SchoolApi.Database.Seeds;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolApi.Database
{
    public static class DataSeed
    {
        public static void DoSeed(SchoolApiContext context)
        {
            StudentSeed.DoSeed(context);
            InstructorSeed.DoSeed(context);
            DepartmentSeed.DoSeed(context);
            CourseSeed.DoSeed(context);
            OfficeAssignmentSeed.DoSeed(context);
            CourseAssignmentSeed.DoSeed(context);
            EnrollmentSeed.DoSeed(context);
        }
    }
}

