import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { StudentComponent } from './components/student/student.component';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { DepartmentComponent } from './components/department/department.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatCardModule } from '@angular/material/card';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { StudentSelectComponent } from './components/student/student-select/student-select.component';
import { MatButtonModule } from '@angular/material/button';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatListModule } from '@angular/material/list';
import { StudentDeleteComponent } from './components/student/student-select/student-delete/student-delete.component';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { StudentAddComponent } from './components/student/student-add/student-add.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { OwlFormFieldModule, OwlInputModule, OwlSelectModule   } from 'owl-ng';
import { DepartmentAddComponent } from './components/department/department-add/department-add.component';
import { DepartmentDeleteComponent } from './components/department/department-delete/department-delete.component';
import { DepartmentDetailsEditComponent } from './components/department/department-details-edit/department-details-edit.component';
import { CourseComponent } from './components/course/course.component';
import { CourseAddComponent } from './components/course/course-add/course-add.component';
import { CourseDeleteComponent } from './components/course/course-delete/course-delete.component';
import { CourseEditComponent } from './components/course/course-edit/course-edit.component';
import { InstructorComponent } from './components/instructor/instructor.component';
import { InstructorAddComponent } from './components/instructor/instructor-add/instructor-add.component';
import { MatSelectModule } from '@angular/material/select';
import { InstructorDeleteComponent } from './components/instructor/instructor-delete/instructor-delete.component';
import { InstructorEditComponent } from './components/instructor/instructor-edit/instructor-edit.component';

  

@NgModule({
  declarations: [
        AppComponent,
        StudentComponent,
        DepartmentComponent,
        StudentSelectComponent,
        StudentDeleteComponent,
        StudentAddComponent,
        DepartmentAddComponent,
        DepartmentDeleteComponent,
        DepartmentDetailsEditComponent,
        CourseComponent,
        CourseAddComponent,
        CourseDeleteComponent,
        CourseEditComponent,
        InstructorComponent,
        InstructorAddComponent,
        InstructorDeleteComponent,
        InstructorEditComponent,
        
  ],
  imports: [
    BrowserModule,
      AppRoutingModule,
      HttpClientModule,
      BrowserAnimationsModule,
      MatSliderModule,
      MatCardModule,
      MatTableModule,
      MatPaginatorModule,
      MatDialogModule,
      MatButtonModule,
      MatExpansionModule,
      MatIconModule,
      MatFormFieldModule,
      MatInputModule,
      MatDividerModule,
      MatListModule,
      MatCheckboxModule,
      FormsModule,
      ReactiveFormsModule,
      MatDatepickerModule,
      MatNativeDateModule,
      OwlDateTimeModule,
      OwlNativeDateTimeModule,
      OwlFormFieldModule,
      OwlInputModule,
      OwlSelectModule,
      MatSnackBarModule,
      MatSelectModule
    ],
    providers: [MatDatepickerModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
