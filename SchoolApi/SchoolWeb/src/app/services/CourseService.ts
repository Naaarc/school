
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseResponse } from '../models/response-models/BaseResponse';
import { StudentsResponse } from '../models/response-models/Student/StudentsResponse';
import { BaseService } from './BaseService';
import { StudentResponse } from '../models/response-models/Student/StudentResponse';
import { BooleanResponse } from '../models/response-models/BooleanResponse';
import { Student } from '../models/Student';
import { CourseResponse } from '../models/response-models/Course/CourseResponse';
import { CoursesResponse } from '../models/response-models/Course/CoursesResponse';
import { Course } from '../models/Course';


@Injectable({
    providedIn: 'root'
})

export class CourseService extends BaseService
{
    private headers: HttpHeaders;

    constructor(http: HttpClient) {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
    }

    async GetAllCourses(): Promise<BaseResponse<CoursesResponse>> {
        const response = await this.baseGetRequest<BaseResponse<CoursesResponse>>('https://localhost:44320/api/Courses/GetCourses', "", this.headers);
        return response;
    }
    async GetCourse(courseId: number): Promise<BaseResponse<CourseResponse>> {
        const parm = "?CourseId=" + courseId;
        const response = await this.baseGetRequest<BaseResponse<CourseResponse>>('https://localhost:44320/api/Courses/GetCourse', parm, this.headers)
        return response;
    }
    async RemoveCourse(courseId: number): Promise<BaseResponse<BooleanResponse>> {
        const parm = "?CourseId=" + courseId;
        const response = await this.baseDeleteRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Courses/RemoveCourse', parm, this.headers);
        console.log(response);
        return response;
    }
    async CreateCourse(course: Course): Promise<BaseResponse<BooleanResponse>> {
        const response = await this.basePostRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Courses/CreateCourse', JSON.stringify(course), this.headers);
        return response;
    }
    async UpdateStudent(course: Course): Promise<BaseResponse<CourseResponse>> {
        const response = await this.basePutRequest<BaseResponse<CourseResponse>>('https://localhost:44320/api/Courses/UpdateCourse', JSON.stringify(course), this.headers);
        return response;
    }

}
