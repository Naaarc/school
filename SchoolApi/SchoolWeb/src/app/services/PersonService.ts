import { extend } from 'webdriver-js-extender';
import { BaseService } from './BaseService';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PersonsResponse } from '../models/response-models/Person/PersonSResponse';
import { BaseResponse } from '../models/response-models/BaseResponse';

@Injectable({
    providedIn: 'root'
})

export class PersonService extends BaseService
{
    private headers: HttpHeaders;
    constructor(http: HttpClient)
    {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
    }
    async getAllPersons(): Promise<BaseResponse<PersonsResponse>> {
        const response = await this.baseGetRequest<BaseResponse<PersonsResponse>>('http://localhost:62989/api/Tests/GetTests', "", this.headers);
        return response;
    }
}
