
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseResponse } from '../models/response-models/BaseResponse';
import { StudentsResponse } from '../models/response-models/Student/StudentsResponse';
import { BaseService } from './BaseService';
import { StudentResponse } from '../models/response-models/Student/StudentResponse';
import { BooleanResponse } from '../models/response-models/BooleanResponse';
import { Student } from '../models/Student';
import { DepartmentsResponse } from '../models/response-models/Department/DepartmentsResponse';
import { InstructorsResponse } from '../models/response-models/Instructor/InstructorsResponse';
import { Instructor } from '../models/Instructor';
import { InstructorResponse } from '../models/response-models/Instructor/InstructorResponse';


@Injectable({
    providedIn: 'root'
})

export class InstructorService extends BaseService
{
    private headers: HttpHeaders;

    constructor(http: HttpClient) {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
    }

    async GetAllInstructors(): Promise<BaseResponse<InstructorsResponse>> {
        console.log("xsd")
        const response = await this.baseGetRequest<BaseResponse<InstructorsResponse>>('https://localhost:44320/api/Instructors/GetInstructors', "", this.headers);
        return response;
    }
    async GetInstructor(instructorId: number): Promise<BaseResponse<InstructorResponse>> {
        const param = "?InstructorId=" + instructorId; 
        const response = await this.baseGetRequest<BaseResponse<InstructorResponse>>('https://localhost:44320/api/Instructors/GetInstructor', param, this.headers)
        return response;
    }
    async RemoveStudent(instructorId: number): Promise<BaseResponse<BooleanResponse>> {
        console.log(instructorId,'serws')
        const param = "?InstructorId=" + instructorId;
        const response = await this.baseDeleteRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Instructors/RemoveInstructor', param, this.headers);
        console.log(response);
        return response;
    }
    async CreateStudent(instructor: Instructor): Promise<BaseResponse<BooleanResponse>> {
        const response = await this.basePostRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Instructors/CreateInstructor', JSON.stringify(instructor), this.headers);
        return null;
    }
    async UpdateStudent(instructor: Instructor): Promise<BaseResponse<InstructorResponse>> {
        console.log(instructor,'serwis')
        const response = await this.basePutRequest<BaseResponse<InstructorResponse>>('https://localhost:44320/api/Instructors/UpdateInstructor', JSON.stringify(instructor), this.headers);
        return response;
    }

}
