
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseResponse } from '../models/response-models/BaseResponse';
import { StudentsResponse } from '../models/response-models/Student/StudentsResponse';
import { BaseService } from './BaseService';
import { StudentResponse } from '../models/response-models/Student/StudentResponse';
import { BooleanResponse } from '../models/response-models/BooleanResponse';
import { Student } from '../models/Student';


@Injectable({
    providedIn: 'root'
})

export class StudentService extends BaseService
{
    private headers: HttpHeaders;

    constructor(http: HttpClient) {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
    }

    async GetAllStudents(): Promise<BaseResponse<StudentsResponse>> {
        const response = await this.baseGetRequest<BaseResponse<StudentsResponse>>('https://localhost:44320/api/Students/GetStudents', "", this.headers);
        return response;
    }
    async GetStudent(studentId: number): Promise<BaseResponse<StudentResponse>> {
        const parm = "?ID=" + studentId;
        const response = await this.baseGetRequest<BaseResponse<StudentResponse>>('https://localhost:44320/api/Students/GetStudent', parm, this.headers)
        return response;
    }
    async RemoveStudent(studentId: number): Promise<BaseResponse<BooleanResponse>> {
        const parm = "?ID=" + studentId;
        const response = await this.baseDeleteRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Students/RemoveStudent', parm, this.headers);
        console.log(response);
        return response;
    }
    async CreateStudent(student: Student): Promise<BaseResponse<BooleanResponse>> {
        console.log(student);
        const response = await this.basePostRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Students/CreateStudent', JSON.stringify(student), this.headers);
        return null;
    }
    async UpdateStudent(student: Student): Promise<BaseResponse<StudentResponse>> {
        const response = await this.basePutRequest<BaseResponse<StudentResponse>>('https://localhost:44320/api/Students/UpdateStudent', JSON.stringify(student), this.headers);
        return response;
    }

}
