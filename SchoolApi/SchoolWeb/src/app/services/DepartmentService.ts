import { extend } from 'webdriver-js-extender';
import { BaseService } from './BaseService';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { PersonsResponse } from '../models/response-models/Person/PersonSResponse';
import { BaseResponse } from '../models/response-models/BaseResponse';
import { StudentResponse } from '../models/response-models/Student/StudentResponse';
import { StudentsResponse } from '../models/response-models/Student/StudentsResponse';
import { DepartmentsResponse } from '../models/response-models/Department/DepartmentsResponse';
import { BooleanResponse } from '../models/response-models/BooleanResponse';
import { Department } from '../models/Department';
import { DepartmentResponse } from '../models/response-models/Department/DepartmentResponse';

@Injectable({
    providedIn: 'root'
})

export class DepartmentService extends BaseService
{
    private headers: HttpHeaders;
    constructor(http: HttpClient)
    {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
    }
    async GetAllDepartments(): Promise<BaseResponse<DepartmentsResponse>> {
        const response = await this.baseGetRequest<BaseResponse<DepartmentsResponse>>('https://localhost:44320/api/Departments/GetDepartments', "", this.headers);
        console.log(response);
        return response;
    }
    async GetDepartment(departmentId: number): Promise<BaseResponse<DepartmentResponse>> {
        const parm = "?DepartmentId=" + departmentId;
        const response = await this.baseGetRequest<BaseResponse<DepartmentResponse>>('https://localhost:44320/api/Departments/GetDepartment', parm, this.headers)
        return response;
    }
    async RemoveDepartment(departmentId: number): Promise<BaseResponse<BooleanResponse>> {
        const parm = "?DepartmentId=" + departmentId;
        const response = await this.baseDeleteRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Departments/RemoveDepartment', parm, this.headers);
        console.log(response);
        return response;
    }
    async CreateDepartment(department: Department): Promise<BaseResponse<BooleanResponse>> {
        console.log(department)
        const response = await this.basePostRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Departments/CreateDepartment', JSON.stringify(department), this.headers);
        return null;
    }
    async UpdateDepartment(department: Department): Promise<BaseResponse<DepartmentResponse>> {
        const response = await this.basePutRequest<BaseResponse<DepartmentResponse>>('https://localhost:44320/api/Departments/UpdateDepartment', JSON.stringify(department), this.headers);
        return response;
    }
}
