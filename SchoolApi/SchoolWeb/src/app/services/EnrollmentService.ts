
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BaseResponse } from '../models/response-models/BaseResponse';
import { StudentsResponse } from '../models/response-models/Student/StudentsResponse';
import { BaseService } from './BaseService';
import { StudentResponse } from '../models/response-models/Student/StudentResponse';
import { BooleanResponse } from '../models/response-models/BooleanResponse';
import { Student } from '../models/Student';
import { Enrollment } from '../models/Enrollment';


@Injectable({
    providedIn: 'root'
})

export class EnrollmentService extends BaseService {
    private headers: HttpHeaders;
    private studentId: number;
    private courseId: number;

    constructor(http: HttpClient) {
        super(http);
        this.headers = new HttpHeaders();
        this.headers = this.headers.set('Content-Type', 'application/json');
        this.headers = this.headers.set('Accept', 'application/json');
    }

    async CreateEnrollment(enrollment: Array<Enrollment>): Promise<BaseResponse<BooleanResponse>> {
        console.log(enrollment);
        const response = await this.basePostRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Enrollments/CreateEnrollment', JSON.stringify(enrollment), this.headers);
        return response;
    }

    async RemoveEnrollment(enrollment: Array<Enrollment>): Promise<BaseResponse<BooleanResponse>> {
        console.log('wchodze!!!!!!!!!!!!!!!!!!!!!!!', enrollment)
        let response;
        for (let i of enrollment) {
            let parm = "?StudentId=" + i.studentId + "&CourseId=" + i.courseId + "&Flag=" + i.flag;
            response = await this.baseDeleteRequest<BaseResponse<BooleanResponse>>('https://localhost:44320/api/Enrollments/RemoveEnrollment', parm, this.headers);
        }
        return response;
    }

}
