import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentComponent } from './components/student/student.component';
import { DepartmentComponent } from './components/department/department.component';
import { StudentSelectComponent } from './components/student/student-select/student-select.component';
import { StudentDeleteComponent } from './components/student/student-select/student-delete/student-delete.component';
import { StudentAddComponent } from './components/student/student-add/student-add.component';
import { DepartmentAddComponent } from './components/department/department-add/department-add.component';
import { DepartmentDeleteComponent } from './components/department/department-delete/department-delete.component';
import { DepartmentDetailsEditComponent } from './components/department/department-details-edit/department-details-edit.component';
import { CourseComponent } from './components/course/course.component';
import { CourseAddComponent } from './components/course/course-add/course-add.component';
import { CourseDeleteComponent } from './components/course/course-delete/course-delete.component';
import { CourseEditComponent } from './components/course/course-edit/course-edit.component';
import { InstructorComponent } from './components/instructor/instructor.component';
import { InstructorAddComponent } from './components/instructor/instructor-add/instructor-add.component';
import { InstructorDeleteComponent } from './components/instructor/instructor-delete/instructor-delete.component';
import { InstructorEditComponent } from './components/instructor/instructor-edit/instructor-edit.component';

const routes: Routes = [
    //students
    { path: "student", component: StudentComponent },
    { path: "student-select/:id", component: StudentSelectComponent },
    { path: "student-delete/:id", component: StudentDeleteComponent },
    { path: "student-add", component: StudentAddComponent },
    //departments
    { path: "department", component: DepartmentComponent },
    { path: "department-add", component: DepartmentAddComponent },
    { path: "department-details/:id", component: DepartmentDetailsEditComponent },
    { path: "department-delete/:id", component: DepartmentDeleteComponent },
    //courses
    { path: "course", component: CourseComponent },
    { path: "course-add", component: CourseAddComponent },
    { path: "course-delete/:id", component: CourseDeleteComponent },
    { path: "course-edit/:id", component: CourseEditComponent },
    //instructors
    { path: "instructor", component: InstructorComponent },
    { path: "instructor-add", component: InstructorAddComponent },
    { path: "instructor-delete/:id", component: InstructorDeleteComponent },
    { path: "instructor-edit/:id", component: InstructorEditComponent }
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
