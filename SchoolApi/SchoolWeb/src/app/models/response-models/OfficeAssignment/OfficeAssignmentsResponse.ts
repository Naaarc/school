
import { OfficeAssignment } from '../../OfficeAssignment';

export class OfficeAssignmentsResponse {
    officeAssignments: Array<OfficeAssignment>;

}
