import { Instructor } from '../../Instructor';

export class InstructorsResponse {
    instructors: Array<Instructor>;

}
