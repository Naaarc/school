
import { CourseAssignment } from '../../CourseAssignment';

export class CourseAssignmentsResponse {
    courseAssignments: Array<CourseAssignment>;

}
