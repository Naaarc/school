import { DecimalPipe } from '@angular/common';
import { Instructor } from './Instructor';
import { Course } from './Course';

export class Department {
    constructor() { };

    departmentId: number;
    name: string;
    budget: number;
    startDate: Date;
    instructorId: number;

    administrator: Instructor;
    courses: Course[];
}
