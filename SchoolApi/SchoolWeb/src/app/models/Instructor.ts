import { OfficeAssignment } from './OfficeAssignment';
import { Person } from './Person';
import { CourseAssignment } from './CourseAssignment';

export class Instructor extends Person{

    hireDate: Date;
    courseAssignments: CourseAssignment[];
    officeAssignment: OfficeAssignment;
}
