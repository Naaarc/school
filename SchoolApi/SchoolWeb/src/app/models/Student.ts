import { Enrollment } from './Enrollment';
import { extend } from 'webdriver-js-extender';
import { Person } from './Person';
import { Course } from './Course';

export class Student {
    ID: number;
    lastName: string;
    firstMidName: string;
    courses: Course[];
    enrollmentDate: Date;
    Enrollments: Enrollment[];
    //flag: boolean;
}
