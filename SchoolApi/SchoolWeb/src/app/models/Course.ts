import { CourseAssignment } from './CourseAssignment';
import { Enrollment } from './Enrollment';

export class Course {
    constructor() { };

    courseId: number;
    title: string;
    credits: number;
    departmentId: number;
    flag: boolean;
    enrollments: Enrollment[];
    CourseAssignments: CourseAssignment[];
}

