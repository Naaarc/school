import { Course } from './Course';
import { Student } from './Student';

export class Enrollment {
    constructor() { };

    enrollmentId: number;
    studentId: number;
    courseId: number;
    grade: string;

    flag: boolean

    course: Course;
    student: Student;
}
