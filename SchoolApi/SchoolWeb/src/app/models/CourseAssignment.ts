import { Instructor } from './Instructor';
import { Course } from './Course';

export class CourseAssignment {
    constructor() { };

    courseId: number;
    instructorId: number;
    flag: boolean;

    course: Course;
    instructor: Instructor;
}
