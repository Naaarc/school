import { Instructor } from './Instructor';

export class OfficeAssignment {
    constructor() { };

    instructorId: number;
    location: string;
    instructor: Instructor;
}
