import { Component, OnInit } from '@angular/core';
import { Student } from '../../../models/Student';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { StudentService } from '../../../services/StudentService';
import { DatePipe } from '@angular/common';
import { MatSnackBar, MatDialogRef } from '@angular/material';
import { StudentComponent } from '../student.component';
import { Router, ActivatedRoute } from '@angular/router';



@Component({
    selector: 'app-student-add',
    templateUrl: './student-add.component.html',
    styleUrls: ['./student-add.component.css']
})
export class StudentAddComponent implements OnInit {
    studentModel: Student;
    
    constructor(
        public dialogRef: MatDialogRef<StudentComponent>,
        public studentService: StudentService,
        private _snackBar: MatSnackBar,
        public _router: Router,
        public route: ActivatedRoute) { }

    ngOnInit() {
        this.studentModel = new Student();
    }

    private async addStudent() {
        await this.studentService.CreateStudent(this.studentModel);
        this._snackBar.open('Student Added!', '', {
            duration: 1500
        });
    }
    onAddStudent() {
        this.addStudent();
        this._router.navigateByUrl('student');
        this.dialogRef.close();
    }
    isDisabledButton(studentValue: Student) {

        if (!studentValue.firstMidName || !studentValue.lastName) {
            return true;
        }
        return false;
    }
}
