import { Component, OnInit, Inject } from '@angular/core';
import { StudentService } from '../../../services/StudentService';
import { Student } from '../../../models/Student';
import { MAT_DIALOG_DATA, MatDialog, MatDialogRef } from '@angular/material';
import { StudentDeleteComponent } from './student-delete/student-delete.component';
import { CourseService } from '../../../services/CourseService';
import { Course } from '../../../models/Course';
import { FormsModule } from '@angular/forms';
import { EnrollmentService } from '../../../services/EnrollmentService';
import { Enrollment } from '../../../models/Enrollment';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-student-select',
    templateUrl: './student-select.component.html',
    styleUrls: ['./student-select.component.css']
})
export class StudentSelectComponent implements OnInit {
    checked: boolean = false;
    studentId: number;
    student: Student;

    studentCourse: Array<Course> = [];
    courses: Array<Course> = [];

    enrollmentModelTab: Array<Enrollment> = [];
    enrollmentModel: Enrollment;
    
    mySubscription: any;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<StudentSelectComponent>,
        private studentService: StudentService,
        private coursesService: CourseService,
        public dialog: MatDialog,
        public enrollmentService: EnrollmentService,
        public _router: Router,
        public route: ActivatedRoute
    ) {
        if (data != null) {
            this.studentId = data.studentId;
        }
       
       
    }
    
    ngOnInit() {
        this.enrollmentModel = new Enrollment();
        this.enrollmentModelTab = new Array<Enrollment>();
        this.student = new Student();
        this.courses = new Array<Course>();
        this.studentCourse = new Array<Course>();
        this.getAllCourses();
    }

    private async getStudent() {
        const studentResponse = await this.studentService.GetStudent(this.studentId);
        this.student = studentResponse.data.student;
        this.studentCourse = this.student.courses;
        console.log(this.courses, this.studentCourse, "KURSY I STUDENT KURS")
        this.studentCourseFlag(this.courses, this.studentCourse);
        
    }

    onDelete(id: number) {
        this.dialog.open(StudentDeleteComponent, {
            height: "40%",
            width: "45%",
            data: { studentId: id }
        });
        this.dialogRef.close();
    }

    private async getAllCourses() {
        const courseResponse = await this.coursesService.GetAllCourses();
        this.courses = courseResponse.data.courses;
        this.getStudent();

       // return this.courses;
    }

    studentCourseFlag(courses: Course[], studentCourse: Course[]) {
        for (var i of courses) {
            for (var y of studentCourse) {
                if (i.courseId == y.courseId) {
                    i.flag = true;
                }
            }
        }
    }
    onEditClick(studentId: number) {
        this.student.ID = studentId;
        this.UpdateStudent();
        this.RemoveEnrollment();
        this.CreateEnrollment();
        this._router.navigateByUrl('student');
        this.dialogRef.close();
    }
    private async UpdateStudent() {
        await this.studentService.UpdateStudent(this.student)
    }
    private async CreateEnrollment() {
        console.log(this.enrollmentModelTab,'create')
        await this.enrollmentService.CreateEnrollment(this.enrollmentModelTab);
        
    }
    private async RemoveEnrollment() {
        console.log(this.enrollmentModelTab, 'remove')
        await this.enrollmentService.RemoveEnrollment(this.enrollmentModelTab);
    }
    ChoosenCourse(courseId: any, studentId: any, isChecked: boolean) {
        //this.checked = !flag;
        let flag = !isChecked;

        //way to dont ovverwrite items in list
        var copy = Object.assign({ courseId, studentId, flag });
        this.enrollmentModelTab.push(copy);
    }
}
