import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { StudentService } from '../../../../services/StudentService';
import { Student } from '../../../../models/Student';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-student-delete',
    templateUrl: './student-delete.component.html',
    styleUrls: ['./student-delete.component.css']
})
export class StudentDeleteComponent implements OnInit {

    studentId: number;
    student: Student;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<StudentDeleteComponent>,
        private studentService: StudentService,
        public _router: Router,
        public route: ActivatedRoute) {
        if (data != null) {
            this.studentId = data.studentId;
        }
    }

    ngOnInit() {
        this.student = new Student();
        this.getStudent(this.studentId);
    }

    private async getStudent(studentId: number) {
        const studentResponse = await this.studentService.GetStudent(this.studentId);
        this.student = studentResponse.data.student;
    }
    private async RemoveStudent() {
        console.log(this.studentId);
        await this.studentService.RemoveStudent(this.studentId);
    }
    onDeleteClick() {
        this.RemoveStudent();
        this._router.navigateByUrl('student');
        this.dialogRef.close();
        location.reload();
    }
}
