import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../../services/StudentService';
import { Student } from '../../models/Student';
import { MatTableDataSource, MatDialog, MatPaginator } from '@angular/material';
import { StudentSelectComponent } from './student-select/student-select.component';
import { StudentAddComponent } from './student-add/student-add.component';

@Component({
  selector: 'app-student',
  templateUrl: './student.component.html',
  styleUrls: ['./student.component.css']
})
export class StudentComponent implements OnInit {
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
    //@ViewChild(MatSort, { static: true }) sort: MatSort;

    displayedColumns: string[] = ['name', 'lastName', 'action'];
    studentsList: MatTableDataSource<Student>;
    students: Array<Student> = [];
    constructor(private _router: Router, private studenService: StudentService, public dialog: MatDialog,) {}

    ngOnInit() {
        this.students = new Array<Student>();
        this.studentsList = new MatTableDataSource<Student>();
        this.getStudentList();
    }
    private async getStudentList() {
        const studentResponse = await this.studenService.GetAllStudents();
        this.students = studentResponse.data.students
        console.log(this.students);
        this.studentsList = new MatTableDataSource<Student>(this.students);
        this.studentsList.paginator = this.paginator;
    }

     onSelect(id: number) {
         this.dialog.open(StudentSelectComponent,
             {
                 height: "45%",
                 width: "70%",
                 data: {studentId: id}
             });
     }

    onAddStudent() {
        this.dialog.open(StudentAddComponent,
            {
                height: "45%",
                width: "70%"
            })
    }
}
