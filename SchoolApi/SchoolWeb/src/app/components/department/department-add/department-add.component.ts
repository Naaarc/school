import { Component, OnInit } from '@angular/core';
import { Department } from '../../../models/Department';
import { DepartmentService } from '../../../services/DepartmentService';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';
import { InstructorService } from '../../../services/InstructorService';
import { Instructor } from '../../../models/Instructor';
import { MatDialogRef } from '@angular/material';
import { DepartmentComponent } from '../department.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-department-add',
    templateUrl: './department-add.component.html',
    styleUrls: ['./department-add.component.css']
})
export class DepartmentAddComponent implements OnInit {
    departmentModel: Department;

    instructors: Array<Instructor> = [];

    constructor(
        public departmentService: DepartmentService,
        public instructorService: InstructorService,
        public dialogRef: MatDialogRef<DepartmentComponent>,
        public _router: Router
    ) { }

    ngOnInit() {
        this.departmentModel = new Department();
        this.getInstructors();
    }

    private async getInstructors() {
        const instructorResponse = await this.instructorService.GetAllInstructors();
        this.instructors = instructorResponse.data.instructors;
    }
    private async addDepartment() {
        await this.departmentService.CreateDepartment(this.departmentModel);
    }
    onAddDepartment() {
        this.addDepartment();
        this._router.navigateByUrl('department');
        this.dialogRef.close();
    }

}
