import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DepartmentDetailsEditComponent } from './department-details-edit.component';

describe('DepartmentDetailsEditComponent', () => {
  let component: DepartmentDetailsEditComponent;
  let fixture: ComponentFixture<DepartmentDetailsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DepartmentDetailsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DepartmentDetailsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
