import { Component, OnInit, Inject } from '@angular/core';
import { DepartmentService } from '../../../services/DepartmentService';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Department } from '../../../models/Department';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';
import { InstructorService } from '../../../services/InstructorService';
import { Instructor } from '../../../models/Instructor';
@Component({
    selector: 'app-department-details-edit',
    templateUrl: './department-details-edit.component.html',
    styleUrls: ['./department-details-edit.component.css']
})
export class DepartmentDetailsEditComponent implements OnInit {
    //departments
    departmentId: number;
    department: Department;

    //instructors
    instructors: Array<Instructor> = [];

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<DepartmentDetailsEditComponent>,
        public departmentService: DepartmentService,
        public instructorService: InstructorService
    ) {
        if (data != null) {
            this.departmentId = data.departmentId
        }
    }

    ngOnInit() {
        this.department = new Department();
        this.getDepartment();
        this.getInstructors();
    }

    private async getDepartment() {
        const departmentResponse = await this.departmentService.GetDepartment(this.departmentId);
        this.department = departmentResponse.data.department;
        console.log(this.department);
    }

    private async getInstructors() {
        const instructorsResponse = await this.instructorService.GetAllInstructors();
        this.instructors = instructorsResponse.data.instructors;
    }

    private async updateDepartment() {
        await this.departmentService.UpdateDepartment(this.department);
        
    }

    onEditClick(departmentId: number) {
        this.department.departmentId = departmentId;
        this.updateDepartment();
    }

}
