import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { DepartmentService } from '../../services/DepartmentService';
import { Department } from '../../models/Department';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { DepartmentAddComponent } from './department-add/department-add.component';
import { DepartmentDeleteComponent } from './department-delete/department-delete.component';
import { DepartmentDetailsEditComponent } from './department-details-edit/department-details-edit.component';

@Component({
    selector: 'app-department',
    templateUrl: './department.component.html',
    styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    displayedColumns: string[] = ['departmentName', 'budget', 'action']

    departmentsList: MatTableDataSource<Department>;
    departments: Array<Department> = [];

    constructor(private _router: Router, private departmentService: DepartmentService, public dialog: MatDialog) { }

    ngOnInit() {
        this.departments = new Array<Department>();
        this.departmentsList = new MatTableDataSource<Department>();
        this.getDepartmentList();

    }
    private async getDepartmentList() {
        const studentResponse = await this.departmentService.GetAllDepartments();
        this.departments = studentResponse.data.departments
        console.log(this.departments)
        this.departmentsList = new MatTableDataSource<Department>(this.departments);
        this.departmentsList.paginator = this.paginator;
    }

    onAddDepartment() {
        this.dialog.open(DepartmentAddComponent,
            {
                height: "50%",
                width: "70%"
            })
    }

    onEdit(departmentId: number) {
        this.dialog.open(DepartmentDetailsEditComponent,
            {
                height: "50%",
                width: "70%",
                data: { departmentId: departmentId}
            })
    }

    onDelete(departmentId: number) {
        this.dialog.open(DepartmentDeleteComponent,
            {
                height: "50%",
                width: "70%",
                data: { departmentId: departmentId }
            })
    }
     
}
