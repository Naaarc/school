import { Component, OnInit, Inject } from '@angular/core';
import { Department } from '../../../models/Department';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DepartmentService } from '../../../services/DepartmentService';
import { Router } from '@angular/router';

@Component({
    selector: 'app-department-delete',
    templateUrl: './department-delete.component.html',
    styleUrls: ['./department-delete.component.css']
})
export class DepartmentDeleteComponent implements OnInit {
    departmentId: number;
    department: Department;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<DepartmentDeleteComponent>,
        public _router: Router,
        private departmentService: DepartmentService)
    {
        if (data != null) {
            this.departmentId = data.departmentId
        }
    }

    ngOnInit() {
        this.department = new Department();
        this.getDepartment();
    }

    private async getDepartment() {
        const departmentResponse = await this.departmentService.GetDepartment(this.departmentId);
        this.department = departmentResponse.data.department;
        console.log(this.department);
    }
    private async RemoveDepartment() {
        await this.departmentService.RemoveDepartment(this.departmentId);
    }

    onDeleteclick(departmentId: number) {
        this.RemoveDepartment();
        this._router.navigateByUrl('department');
        this.dialogRef.close();
    }
}
