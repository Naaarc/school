import { Component, OnInit, inject, Inject } from '@angular/core';
import { InstructorService } from '../../../services/InstructorService';
import { CourseService } from '../../../services/CourseService';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { Instructor } from '../../../models/Instructor';
import { Course } from '../../../models/Course';
import { InstructorComponent } from '../instructor.component';
import { Router } from '@angular/router';

@Component({
    selector: 'app-instructor-edit',
    templateUrl: './instructor-edit.component.html',
    styleUrls: ['./instructor-edit.component.css']
})
export class InstructorEditComponent implements OnInit {

    //instructors
    instructorId: number;
    instructor: Instructor;

    //courses
    courses: Array<Course> = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<InstructorComponent>,
        public _router: Router,
        public instructorService: InstructorService,
        public courseService: CourseService) {
        if (data != null) {
            this.instructorId = data.instructorId;
        }
    }

    ngOnInit() {
        this.instructor = new Instructor();
        this.courses = new Array<Course>();
        this.getInstructor();
    }

    private async getInstructor() {
        const instructorResponse = await this.instructorService.GetInstructor(this.instructorId);
        this.instructor = instructorResponse.data.instructor;
        console.log(this.instructor);
        this.getAllCourse();
    }

    private async getAllCourse() {
        const courseResponse = await this.courseService.GetAllCourses();
        this.courses = courseResponse.data.courses;
        this.InstructorCourses();
    }

    private async UpdateInstructor() {
        await this.instructorService.UpdateStudent(this.instructor)
    }
    onSave() {
        this.UpdateInstructor();
        this._router.navigateByUrl('instructor');
        this.dialogRef.close();
    }
    InstructorCourses() {
        for (let i of this.courses) {
            for (let y of this.instructor.courseAssignments) {
                if (i.courseId == y.courseId) {
                    i.flag = true;
                    y.flag = true;
                }
            }
        }
    }
    ChoosenCourse(courseId: any, instructorId: any, isChecked: boolean) {

        let flag = !isChecked;
        for (let i of this.instructor.courseAssignments) {
            if (i.courseId == courseId) {
                i.flag = flag
            }
        }
        //way to dont ovverwrite items in a list
        var copy = Object.assign({ courseId, instructorId, flag });
        this.instructor.courseAssignments.push(copy);

        console.log("po kliknieciu", this.instructor);
    }
    
}
