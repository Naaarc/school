import { Component, OnInit, Inject } from '@angular/core';
import { Instructor } from '../../../models/Instructor';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { InstructorComponent } from '../instructor.component';
import { InstructorService } from '../../../services/InstructorService';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
    selector: 'app-instructor-delete',
    templateUrl: './instructor-delete.component.html',
    styleUrls: ['./instructor-delete.component.css']
})
export class InstructorDeleteComponent implements OnInit {
    instructorId: number;
    instructor: Instructor;

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public dialogRef: MatDialogRef<InstructorComponent>,
        public instructorService: InstructorService,
        public _router: Router,
        public route: ActivatedRoute
    ) {
        if (data != null) {
            this.instructorId = data.instructorId;
        }
    }

    ngOnInit() {
        this.instructor = new Instructor();
        this.getInstructor();
    }

    private async getInstructor() {
        const instructorResponse = await this.instructorService.GetInstructor(this.instructorId);
        this.instructor = instructorResponse.data.instructor;
        console.log(this.instructor)
    }
    private async removeInstructor() {
        await this.instructorService.RemoveStudent(this.instructorId);
    }

    onDeleteClick() {
        this.removeInstructor();
        this._router.navigateByUrl('instructor');
        this.dialogRef.close();
    }
}
