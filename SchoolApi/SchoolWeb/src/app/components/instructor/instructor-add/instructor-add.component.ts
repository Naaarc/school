import { Component, OnInit } from '@angular/core';
import { Instructor } from '../../../models/Instructor';
import { InstructorComponent } from '../instructor.component';
import { InstructorService } from '../../../services/InstructorService';
import { MatSnackBar, MatDialogRef } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { CourseService } from '../../../services/CourseService';
import { Course } from '../../../models/Course';

@Component({
    selector: 'app-instructor-add',
    templateUrl: './instructor-add.component.html',
    styleUrls: ['./instructor-add.component.css']
})
export class InstructorAddComponent implements OnInit {
    //instructors
    instructorModel: Instructor;

    //courses
    courses: Array<Course> = [];

    constructor(
        public dialogRef: MatDialogRef<InstructorComponent>,
        public instructorService: InstructorService,
        public courseService: CourseService,
        private _snackBar: MatSnackBar,
        public _router: Router,
        public route: ActivatedRoute
    ) { }

    ngOnInit() {
        this.instructorModel = new Instructor();
        this.courses = new Array<Course>();
        this.getAllCourses();
    }
    //instructors
    private async addInstructor() {
        await this.instructorService.CreateStudent(this.instructorModel);
        this._snackBar.open('Instructor Added', '', {
            duration: 1500
        });
    }

    onAddInstructor() {
        this.addInstructor();
        this._router.navigateByUrl('instructor');
        this.dialogRef.close();
    }

    //courses
    private async getAllCourses() {
        const courseResponse = await this.courseService.GetAllCourses();
        this.courses = courseResponse.data.courses;
    }
}
