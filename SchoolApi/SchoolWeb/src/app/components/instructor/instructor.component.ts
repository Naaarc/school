import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatTableDataSource, MatDialog } from '@angular/material';
import { Instructor } from '../../models/Instructor';
import { InstructorService } from '../../services/InstructorService';
import { InstructorAddComponent } from './instructor-add/instructor-add.component';
import { InstructorDeleteComponent } from './instructor-delete/instructor-delete.component';
import { InstructorEditComponent } from './instructor-edit/instructor-edit.component';

@Component({
    selector: 'app-instructor',
    templateUrl: './instructor.component.html',
    styleUrls: ['./instructor.component.css']
})
export class InstructorComponent implements OnInit {
    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

    displayedColumns: string[] = ['name', 'lastName', 'action']

    //instructors
    instructorsList: MatTableDataSource<Instructor>;
    instructors: Array<Instructor> = [];

    constructor(private instructorService: InstructorService, public dialog: MatDialog) { }

    ngOnInit() {
        this.instructors = new Array<Instructor>();
        this.instructorsList = new MatTableDataSource<Instructor>();
        this.getAllInstructors();
    }

    private async getAllInstructors() {
        const instructorsResponse = await this.instructorService.GetAllInstructors();
        this.instructors = instructorsResponse.data.instructors;
        console.log(this.instructors)
        this.instructorsList = new MatTableDataSource<Instructor>(this.instructors);
        this.instructorsList.paginator = this.paginator;
    }

    onAddInstructor() {
        this.dialog.open(InstructorAddComponent,
            {
                height: "65%%",
                width: "80%"
            })
    }

    onDeleteInstructor(instructorId: number) {
        
        this.dialog.open(InstructorDeleteComponent,
            {
                height: "60%",
                width: "80%",
                data: { instructorId: instructorId }
            });
    }

    onEditInstructor(instructorId: number) {
        this.dialog.open(InstructorEditComponent,
            {
                height: "70%",
                width: "85%",
                data: { instructorId: instructorId}
            })
    }
}
