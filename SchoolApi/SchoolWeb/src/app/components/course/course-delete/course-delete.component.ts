import { Component, OnInit, Inject } from '@angular/core';
import { Course } from '../../../models/Course';
import { MAT_DIALOG_DATA } from '@angular/material';
import { CourseService } from '../../../services/CourseService';

@Component({
    selector: 'app-course-delete',
    templateUrl: './course-delete.component.html',
    styleUrls: ['./course-delete.component.css']
})
export class CourseDeleteComponent implements OnInit {
    courseId: number;
    course: Course;

    constructor(@Inject(MAT_DIALOG_DATA) public data: any, public courseService: CourseService)
    {
        if (data != null) {
            this.courseId = data.courseId;
        }
    }

    ngOnInit() {
        this.course = new Course();
        this.getCourse(this.courseId)
    }

    private async getCourse(courseId: number) {
        const courseResponse = await this.courseService.GetCourse(courseId);
        this.course = courseResponse.data.course
        console.log(this.course);
    }

    private async Remove() {
        await this.courseService.RemoveCourse(this.courseId)
    }

    onDeleteClick() {
        this.Remove();
    }

}
