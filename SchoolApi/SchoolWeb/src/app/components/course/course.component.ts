import { Component, OnInit, ViewChild } from '@angular/core';
import { CourseService } from '../../services/CourseService';
import { Course } from '../../models/Course';
import { MatTableDataSource, MatPaginator, MatDialog } from '@angular/material';
import { CourseAddComponent } from './course-add/course-add.component';
import { CourseEditComponent } from './course-edit/course-edit.component';
import { CourseDeleteComponent } from './course-delete/course-delete.component';

@Component({
    selector: 'app-course',
    templateUrl: './course.component.html',
    styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {

    @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator
    displayedColumns: string[] = ['title', 'credits', 'department' ,'action']

    courseList: MatTableDataSource<Course>;

    //courses
    courses: Array<Course> = [];

    constructor(public courseService: CourseService, public dialog: MatDialog) { }

    ngOnInit() {
        this.courses = new Array<Course>();
        this.getAllCourses();
    }

    private async getAllCourses() {
        const coursesResponse = await this.courseService.GetAllCourses();
        this.courses = coursesResponse.data.courses;
        this.courseList = new MatTableDataSource<Course>(this.courses);
        this.courseList.paginator = this.paginator;
    }

    onAddCourse() {
        this.dialog.open(CourseAddComponent,
            {
                height: "40%",
                width: "70%"
            })
    }

    onEdit(id: number) {
        this.dialog.open(CourseEditComponent,
            {
                height: "45%",
                width: "70%",
                data: { courseId: id }
            })
    }

    onDelete(id: number) {
        this.dialog.open(CourseDeleteComponent,
            {
                height: "50%",
                width: "70%",
                data: {courseId: id}
            })
    }
}
