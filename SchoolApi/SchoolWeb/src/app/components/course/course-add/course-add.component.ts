import { Component, OnInit } from '@angular/core';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';
import { Course } from '../../../models/Course';
import { Department } from '../../../models/Department';
import { CourseService } from '../../../services/CourseService';
import { DepartmentService } from '../../../services/DepartmentService';
@Component({
    selector: 'app-course-add',
    templateUrl: './course-add.component.html',
    styleUrls: ['./course-add.component.css']
})
export class CourseAddComponent implements OnInit {
    //courses
    courseModel: Course;

    //departments
    departments: Array<Department> = [];

    constructor(public courseService: CourseService, public departmentService: DepartmentService) { }

    ngOnInit() {
        this.courseModel = new Course;
        this.departments = new Array<Department>();
        this.getAllDepartments();
    }

    private async getAllDepartments() {
        const departmentResponse = await this.departmentService.GetAllDepartments();
        this.departments = departmentResponse.data.departments;
        console.log(this.departments);
    }

    private async CreateCourse() {
        this.courseService.CreateCourse(this.courseModel);
    }

    onAddCourse() {
        this.CreateCourse();
    }

}
