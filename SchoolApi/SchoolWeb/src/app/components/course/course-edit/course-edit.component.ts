import { Component, OnInit, Inject } from '@angular/core';
import { CourseService } from '../../../services/CourseService';
import { Course } from '../../../models/Course';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { DepartmentService } from '../../../services/DepartmentService';
import { Department } from '../../../models/Department';
import { OwlDateTimeModule, OWL_DATE_TIME_FORMATS } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';
import { CourseComponent } from '../course.component';
import { Router } from '@angular/router';
@Component({
    selector: 'app-course-edit',
    templateUrl: './course-edit.component.html',
    styleUrls: ['./course-edit.component.css']
})
export class CourseEditComponent implements OnInit {

    //courses
    courseId: number;
    course: Course;

    //departments
    departments: Array<Department> = [];

    constructor(
        @Inject(MAT_DIALOG_DATA) public data: any,
        public courseService: CourseService,
        public departmentService: DepartmentService,
        public dialogRef: MatDialogRef<CourseComponent>,
        public _router: Router
    )
    {
        if (data != null) {
            this.courseId = data.courseId;
        }
    }

    ngOnInit() {
        this.course = new Course();
        this.departments = new Array<Department>();
        this.getCourse();
        this.getDepartmentsList();
    }

    private async getCourse() {
        const courseResponse = await this.courseService.GetCourse(this.courseId);
        this.course = courseResponse.data.course;
    }

    private async getDepartmentsList() {
        const departmentResponse = await this.departmentService.GetAllDepartments();
        this.departments = departmentResponse.data.departments;
        console.log(this.departments)
    }

    private async Update() {
        await this.courseService.UpdateStudent(this.course)
    }

    onEditClick() {
        this.Update();
        this._router.navigateByUrl('course');
        this.dialogRef.close();
    }

}
