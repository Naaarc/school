﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels;
using SchoolApi.ViewModels.VModels.VMDepartment;
using SchoolApi.ViewModels.VModels.VMDepartment.Request;
using SchoolApi.ViewModels.VModels.VMDepartment.Response;

namespace SchoolApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Departments")]
    public class DepartmentsController : ControllerBase
    {
        private readonly IDepartmentService _departmentService;
        public DepartmentsController(IDepartmentService departmentService)
        {
            _departmentService = departmentService;
        }

        [Route("GetDepartments")]
        [HttpGet]
        public BaseResponse<VMGetDepartmentListResponse> GetDepartments(VMGetDepartmentListRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetDepartmentListResponse>.SetError("Request body is empty");
            }
            try
            {
                List<VMDepartmentList> result = _departmentService.GetAll(vmRequest);
                VMGetDepartmentListResponse responseObject = VMDepartmentList.ToResponse(result);
                BaseResponse<VMGetDepartmentListResponse> response = BaseResponse<VMGetDepartmentListResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetDepartmentListResponse>.SetError(exc);
            }

        }


        [Route("GetDepartment")]
        [HttpGet]
        public BaseResponse<VMGetDepartmentResponse> GetDepartment(VMGetRemoveDepartmentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetDepartmentResponse>.SetError("Request body is empty");
            }
            try
            {
                VMDepartmentDetails result = _departmentService.Get(vmRequest);
                VMGetDepartmentResponse responseObject = VMDepartmentList.ToResponse(result);
                BaseResponse<VMGetDepartmentResponse> response = BaseResponse<VMGetDepartmentResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetDepartmentResponse>.SetError(exc);
            }
        }

        [Route("CreateDepartment")]
        [HttpPost]
        public BaseResponse<VMGetDepartmentResponse> Create([FromBody]VMCreateDepartmentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetDepartmentResponse>.SetError("Request body is empty");
            }

            try
            {
                VMDepartmentDetails result = _departmentService.Create(vmRequest);
                VMGetDepartmentResponse responseObject = VMDepartmentList.ToResponse(result);
                BaseResponse<VMGetDepartmentResponse> response = BaseResponse<VMGetDepartmentResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<VMGetDepartmentResponse>.SetError(ex);
            }
        }

        [Route("RemoveDepartment")]
        [HttpDelete]
        public BaseResponse<BooleanResponse> Remove(VMGetRemoveDepartmentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<BooleanResponse>.SetError("request body is empty");
            }
            try
            {
                bool result = _departmentService.Remove(vmRequest);
                BooleanResponse responseObject = new BooleanResponse(result);
                BaseResponse<BooleanResponse> response = BaseResponse<BooleanResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<BooleanResponse>.SetError(exc);
            }
        }

        [Route("UpdateDepartment")]
        [HttpPut]
        public BaseResponse<VMGetDepartmentResponse> Update([FromBody]VMUpdateDepartmentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetDepartmentResponse>.SetError("Request body is empty");
            }
            try
            {
                VMDepartmentDetails result = _departmentService.Update(vmRequest);
                VMGetDepartmentResponse responseObject = VMDepartmentList.ToResponse(result);
                BaseResponse<VMGetDepartmentResponse> response = BaseResponse<VMGetDepartmentResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetDepartmentResponse>.SetError(exc);
            }
        }
    }
}
