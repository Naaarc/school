﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SchoolApi.Database;
using SchoolApi.Service.Interfaces;
using SchoolApi.Service.Services;
using SchoolApi.ViewModels;
using SchoolApi.ViewModels.VModels.VMInstructor;
using SchoolApi.ViewModels.VModels.VMInstructor.Request;
using SchoolApi.ViewModels.VModels.VMInstructor.Response;

namespace SchoolApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Instructors")]
    public class InstructorsController : ControllerBase
    {
        private readonly IInstructorService _instructorService;
        private readonly SchoolApiContext _context;
        public InstructorsController(IInstructorService instructorService, SchoolApiContext context)
        {
            _instructorService = instructorService;
            _context = context;
        }

        [Route("GetInstructors")]
        [HttpGet]
        public BaseResponse<VMGetInstructorsListResponse> GetInstructors(VMGetInstructorsListRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetInstructorsListResponse>.SetError("Request body is empty");
            }
            try
            {
                List<VMInstructorList> result = _instructorService.GetAll(vmRequest);
                VMGetInstructorsListResponse responseObject = VMInstructorList.ToResponse(result);
                BaseResponse<VMGetInstructorsListResponse> response = BaseResponse<VMGetInstructorsListResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetInstructorsListResponse>.SetError(exc);
            }
        }

        [Route("GetInstructor")]
        [HttpGet]
        public BaseResponse<VMGetInstructorResponse> GetInstructor(VMRemoveGetInstructorRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetInstructorResponse>.SetError("Request body is empty");
            }
            try
            {
                VMInstructorDetails result = _instructorService.Get(vmRequest);
                VMGetInstructorResponse responseObject = VMInstructorList.ToResponse(result);
                BaseResponse<VMGetInstructorResponse> response = BaseResponse<VMGetInstructorResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetInstructorResponse>.SetError(exc);
            }
        }


        [Route("CreateInstructor")]
        [HttpPost]
        public BaseResponse<VMGetInstructorResponse> Create([FromBody]VMCreateInstructorRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetInstructorResponse>.SetError("Request body is empty");
            }

            try
            {
                VMInstructorDetails result = _instructorService.Create(vmRequest);
                VMGetInstructorResponse responseObject = VMInstructorList.ToResponse(result);
                BaseResponse<VMGetInstructorResponse> response = BaseResponse<VMGetInstructorResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<VMGetInstructorResponse>.SetError(ex);
            }
        }

        [Route("RemoveInstructor")]
        [HttpDelete]
        public BaseResponse<BooleanResponse> Remove(VMRemoveGetInstructorRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<BooleanResponse>.SetError("request body is empty");
            }
            try
            {
                bool result = _instructorService.Remove(vmRequest);
                BooleanResponse responseObject = new BooleanResponse(result);
                BaseResponse<BooleanResponse> response = BaseResponse<BooleanResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<BooleanResponse>.SetError(exc);
            }
        }

        [Route("UpdateInstructor")]
        [HttpPut]
        public BaseResponse<VMGetInstructorResponse> Update([FromBody]VMUpdateInstructorRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetInstructorResponse>.SetError("Request body is empty");
            }
            try
            {
                VMInstructorDetails result = _instructorService.Update(vmRequest);
                VMGetInstructorResponse responseObject = VMInstructorList.ToResponse(result);
                BaseResponse<VMGetInstructorResponse> response = BaseResponse<VMGetInstructorResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetInstructorResponse>.SetError(exc);
            }
        }
    }
}