﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels;
using SchoolApi.ViewModels.VModels.VMCourse;
using SchoolApi.ViewModels.VModels.VMCourse.Request;
using SchoolApi.ViewModels.VModels.VMCourse.Response;

namespace SchoolApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Courses")]
    public class CoursesController : ControllerBase
    {
        private readonly ICourseService _courseService;
        public CoursesController(ICourseService courseService)
        {
            _courseService = courseService;
        }
        [HttpGet]
        [Route("GetCourses")]
        public BaseResponse<VMGetCourseListResponse> GetAll(VMGetCourseListRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetCourseListResponse>.SetError("Request body is empty");
            }
            try
            {
                List<VMCourseList> result = _courseService.GetAll(vmRequest);
                VMGetCourseListResponse responseObject = VMCourseList.ToResponse(result);
                BaseResponse<VMGetCourseListResponse> response = BaseResponse<VMGetCourseListResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetCourseListResponse>.SetError(exc);
            }
        }

        [HttpGet]
        [Route("GetCourse")]
        public BaseResponse<VMGetCourseResponse> GetCourse(VMGetRemoveCourseDetailsRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetCourseResponse>.SetError("Request body is empty");
            }
            try
            {
                VMCourseDetails result = _courseService.Get(vmRequest);
                VMGetCourseResponse responseObject = VMCourseList.ToResponse(result);
                BaseResponse<VMGetCourseResponse> response = BaseResponse<VMGetCourseResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetCourseResponse>.SetError(exc);
            }
        }

        [Route("CreateCourse")]
        [HttpPost]
        public BaseResponse<VMGetCourseResponse> Create([FromBody]VMCreateCourseRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetCourseResponse>.SetError("Request body is empty");
            }

            try
            {
                VMCourseDetails result = _courseService.Create(vmRequest);
                VMGetCourseResponse responseObject = VMCourseList.ToResponse(result);
                BaseResponse<VMGetCourseResponse> response = BaseResponse<VMGetCourseResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<VMGetCourseResponse>.SetError(ex);

            }
        }

        [Route("RemoveCourse")]
        [HttpDelete]
        public BaseResponse<BooleanResponse> Remove(VMGetRemoveCourseDetailsRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<BooleanResponse>.SetError("request body is empty");
            }
            try
            {
                bool result = _courseService.Remove(vmRequest);
                BooleanResponse responseObject = new BooleanResponse(result);
                BaseResponse<BooleanResponse> response = BaseResponse<BooleanResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<BooleanResponse>.SetError(exc);
            }
        }

        [Route("UpdateCourse")]
        [HttpPut]
        public BaseResponse<VMGetCourseResponse> Update([FromBody]VMUpdateCourseRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetCourseResponse>.SetError("Request body is empty");
            }
            try
            {
                VMCourseDetails result = _courseService.Update(vmRequest);
                VMGetCourseResponse responseObject = VMCourseList.ToResponse(result);
                BaseResponse<VMGetCourseResponse> response = BaseResponse<VMGetCourseResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMGetCourseResponse>.SetError(exc);
            }
        }
    }
}