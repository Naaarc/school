﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolApi.Service.Interfaces;
using SchoolApi.Service.Services;
using SchoolApi.ViewModels;

namespace SchoolApi.Controllers
{
    
    [Produces("application/json")]
    [Route("api/Seed")]
    public class SeedController : ControllerBase
    {
        private readonly ISeedService _seerdService;

        public SeedController(ISeedService seedService)
        {
            _seerdService = seedService;
        }


        [HttpGet]
        [Route("DoSeed")]
        public BaseResponse<BooleanResponse> DoSeed()
        {
            try
            {
                bool result = _seerdService.Seed();
                BooleanResponse responseObject = new BooleanResponse(result);
                var response = BaseResponse<BooleanResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<BooleanResponse>.SetError(ex);
            }
        }
    }
}