﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels;
using SchoolApi.ViewModels.VModels;
using SchoolApi.ViewModels.VModels.VMCourse.Request;
using SchoolApi.ViewModels.VModels.VMStudent;
using SchoolApi.ViewModels.VModels.VMStudent.Request;
using SchoolApi.ViewModels.VModels.VMStudent.Response;

namespace SchoolApi.Controllers
{

    [Produces("application/json")]
    [Route("api/Students")]
    public class StudentsController : ControllerBase
    {
        private readonly IStudentService _studentService;
        public StudentsController(IStudentService studentService)
        {
            _studentService = studentService;
        }

        [Route("GetStudents")]
        [HttpGet]
        public BaseResponse<VMGetStudentsListResponse> GetStudents(GetStudentsListRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMGetStudentsListResponse>.SetError("Request body is empty");
            }
            try
            {
                List<VMStudentList> result = _studentService.GetAll(vmRequest);
                VMGetStudentsListResponse responseObject = VMStudentList.ToResponse(result);
                BaseResponse<VMGetStudentsListResponse> response = BaseResponse<VMGetStudentsListResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<VMGetStudentsListResponse>.SetError(ex);
            }
        }
        [Route("GetStudent")]
        [HttpGet]
        public BaseResponse<VMStudentResponse> GetStudent(VMGetStudentDetailsRemoveUpdateRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMStudentResponse>.SetError("Request body is empty");
            }
            try
            {
                VMStudentDetails result = _studentService.Get(vmRequest);
                VMStudentResponse responseObject = VMStudentDetails.ToResponse(result);
                BaseResponse<VMStudentResponse> response = BaseResponse<VMStudentResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMStudentResponse>.SetError(exc);
            }

        }

        [Route("CreateStudent")]
        [HttpPost]
        public BaseResponse<VMStudentResponse> Create([FromBody]VMCreateStudentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMStudentResponse>.SetError("Request body is empty");
            }

            try
            {
                VMStudentDetails result = _studentService.Create(vmRequest);
                VMStudentResponse responseObject = VMStudentList.ToResponse(result);
                BaseResponse<VMStudentResponse> response = BaseResponse<VMStudentResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception ex)
            {
                return BaseResponse<VMStudentResponse>.SetError(ex);
            }
        }

        [Route("RemoveStudent")]
        [HttpDelete]
        public BaseResponse<BooleanResponse> Remove(VMGetStudentDetailsRemoveUpdateRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<BooleanResponse>.SetError("request body is empty");
            }
            try
            {
                bool result = _studentService.Remove(vmRequest);
                BooleanResponse responseObject = new BooleanResponse(result);
                BaseResponse<BooleanResponse> response = BaseResponse<BooleanResponse>.SetResponse(responseObject);

                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<BooleanResponse>.SetError(exc);
            }
        }

        [Route("UpdateStudent")]
        [HttpPut]
        public BaseResponse<VMStudentResponse> Update([FromBody]VMUpdateStudentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMStudentResponse>.SetError("Request body is empty");
            }
            try
            {
                VMStudentDetails result = _studentService.Update(vmRequest);
                VMStudentResponse responseObject = VMStudentList.ToResponse(result);
                BaseResponse<VMStudentResponse> response = BaseResponse<VMStudentResponse>.SetResponse(responseObject);
                return response;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMStudentResponse>.SetError(exc);
            }
        }
    }
}