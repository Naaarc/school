﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SchoolApi.Service.Interfaces;
using SchoolApi.ViewModels;
using SchoolApi.ViewModels.VModels.VMEnrollment;
using SchoolApi.ViewModels.VModels.VMEnrollment.Request;
using SchoolApi.ViewModels.VModels.VMEnrollment.Response;

namespace SchoolApi.Controllers
{
    [Route("api/Enrollments")]
    [Produces("application/json")]
    public class EnrollmentsController : ControllerBase
    {
        private readonly IEnrollmentService _enrollmentService;
        public EnrollmentsController(IEnrollmentService enrollmentService)
        {
            _enrollmentService = enrollmentService;
        }

        [Route("CreateEnrollment")]
        [HttpPost]
        public BaseResponse<VMEnrollmentResponse> Create([FromBody]List<VMCreateEnrollmentRequest> vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<VMEnrollmentResponse>.SetError("Request body is empty");
            }

            try
            {
                VMEnrollmentDetails result = _enrollmentService.Create(vmRequest);
                VMEnrollmentResponse responseObject = VMEnrollmentList.ToResponse(result);
                BaseResponse<VMEnrollmentResponse> response = BaseResponse<VMEnrollmentResponse>.SetResponse(responseObject);

                return null;
            }
            catch (Exception exc)
            {
                return BaseResponse<VMEnrollmentResponse>.SetError(exc);
            }
        }

        [Route("RemoveEnrollment")]
        [HttpDelete]
        public BaseResponse<BooleanResponse> Remove(VMRemoveDetailsEnrollmentRequest vmRequest)
        {
            if (vmRequest == null)
            {
                return BaseResponse<BooleanResponse>.SetError("Request body is empty");
            }
            try
            {
                bool result = _enrollmentService.Remove(vmRequest);
                BooleanResponse responseObject = new BooleanResponse(result);
                BaseResponse<BooleanResponse> response = BaseResponse<BooleanResponse>.SetResponse(responseObject);

                return response;
            }
            catch(Exception exc)
            {
                return BaseResponse<BooleanResponse>.SetError(exc);
            }
        }

    }
}